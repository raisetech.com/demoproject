import com.docGov.dto.Consumer;
import com.docGov.dto.FeeCalculationQueueMessage;
import com.docGov.dto.MyClient;

import com.sun.tools.corba.se.idl.constExpr.Equal;
import org.junit.Test;

import java.util.*;
import java.util.logging.Logger;

import java.io.ByteArrayOutputStream;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

public class TheFirstUnitTest {
    Logger logger = Logger.getLogger("my logger");

    @Test
    public void testFilterPoldsWithNu() {
        logger.info("Running a dummyTest");
        List<MyClient> filtered1 = null;
        Set<String> adHocPoids = null;
        assertNull(filtered1);
        assertNull(adHocPoids);
    }

    @Test
    public void testIsCreateInvoice() {
        // Create a mock consumer that returns true for isCreateInvoice method
        MyClient mockConsumer = new MyClient();
        List<String> dfeQueuellessages = new ArrayList<>();
        List<String> fcoQueueltessages = new ArrayList<>();
        dfeQueuellessages.add("test 1");
        fcoQueueltessages.add("test 2");

        List<String> allQueueMessages = new ArrayList<>();

        if (mockConsumer.isCreateInvice()) {
            allQueueMessages.addAll(dfeQueuellessages);
            allQueueMessages.addAll(fcoQueueltessages);
        }
        assertTrue(allQueueMessages.containsAll(dfeQueuellessages));
        assertTrue(allQueueMessages.containsAll(fcoQueueltessages));
    }


    @Test
    public void testLogCOFailure() {
        FeeCalculationQueueMessage mockMessage = new FeeCalculationQueueMessage();
        mockMessage.setProfileId("1");
        mockMessage.setServiceAgreementId("2");

        List<FeeCalculationQueueMessage> fcoQueueMessages = new ArrayList<>();
        fcoQueueMessages.add(mockMessage);

        ByteArrayOutputStream logOutput = new ByteArrayOutputStream();
        Logger logger = Logger.getLogger(TheFirstUnitTest.class.getName());
        logger.addHandler(new StreamHandler(logOutput, new SimpleFormatter()));

        for (FeeCalculationQueueMessage message : fcoQueueMessages) {
            logger.info("POID:" + message.getProfileId() + " SAG:" + message.getServiceAgreementId());
        }

        // Verify that the log message was output as expected
        String expectedLogOutput = "POID:1 SAG:2";
        System.out.println(" logOutput.toString()" + logOutput);
        assertEquals(expectedLogOutput, "POID:1 SAG:2");
    }

    @Test
    public void testFilterClientsByPoid() {
        List<MyClient> allSagEnrolledClients = new ArrayList<>();
        MyClient my = new MyClient();
        my.setName("John");
        my.setClientPoid("100");
        allSagEnrolledClients.add(my);

        my = new MyClient();
        my.setName("Alice");
        my.setClientPoid("200");
        allSagEnrolledClients.add(my);

        my = new MyClient();
        my.setName("Bob");
        my.setClientPoid("300");
        allSagEnrolledClients.add(my);
        System.out.println("allSagEnrolledClients" + allSagEnrolledClients);

        Set<String> adhocPoids = new HashSet<>(Arrays.asList("200", "300"));

        List<MyClient> filtered = filterClientsByPoids(allSagEnrolledClients, adhocPoids);

        assertEquals(2, filtered.size());
        assertEquals("Alice", filtered.get(0).getName());
        assertEquals("Bob", filtered.get(1).getName());
    }

    private List<MyClient> filterClientsByPoids(List<MyClient> allSagEnrolledClients, Set<String> adhocPoids) {
        List<MyClient> filtered = new ArrayList<>();
        if (allSagEnrolledClients != null && adhocPoids != null) {
            System.out.println("ok" + adhocPoids.contains("sec.getClientPoid()") + allSagEnrolledClients.get(0).getClientPoid());
            filtered = allSagEnrolledClients.stream()
                    .filter(sec -> adhocPoids.contains(sec.getClientPoid()))
                    .collect(Collectors.toList());
        }
        return filtered;
    }

//    @Test
//    public void testCreateInvoicesAndAdHocPoids() {
//        Consumer consumer = new Consumer();
//        String[] segments = {"test", "value", "poids=1-2-3"};
//        boolean createInvoices = true;
//        MyClient my = new MyClient();
//        // Verify that the flag is set and ad-hoc poids are extracted and set correctly
//        consumer.setCreateInvoices(!createInvoices);
//        my.setConsumerAdHocPoids(consumer, new HashSet<>());
//        my.createInvoicesAndAdHocPoids(consumer, segments, createInvoices);
//
//        assertTrue(consumer.createInvoices);
//        assertEquals(3, consumer.getAdhocPoids().size());
//        assertTrue(consumer.getAdhocPoids().contains("1"));
//        assertTrue(consumer.getAdhocPoids().contains("2"));
//        assertTrue(consumer.getAdhocPoids().contains("3"));
//
//        // Verify that changing the flag value results in a mutation
//        consumer.setCreateInvoices(createInvoices);
//        MyClass.setConsumerAdHocPoids(consumer, new HashSet<>());
//        MyClass.createInvoicesAndAdHocPoids(consumer, segments, !createInvoices);
//
//        assertFalse(consumer.isCreateInvoice());
//        assertEquals(3, consumer.getAdHocPoids().size());
//        assertTrue(consumer.getAdHocPoids().contains("1"));
//        assertTrue(consumer.getAdHocPoids().contains("2"));
//        assertTrue(consumer.getAdHocPoids().contains("3"));
//
//        // Verify that modifying the ad-hoc poids values results in a mutation
//        consumer.setCreateInvoice(createInvoices);
//        MyClass.setConsumerAdHocPoids(consumer, new HashSet<>());
//        MyClass.createInvoicesAndAdHocPoids(consumer, new String[]{"test", "value", "poids=4-5-6"}, createInvoices);
//
//        assertTrue(consumer.isCreateInvoice());
//        assertEquals(3, consumer.getAdHocPoids().size());
//        assertTrue(consumer.getAdHocPoids().contains("4"));
//        assertTrue(consumer.getAdHocPoids().contains("5"));
//        assertTrue(consumer.getAdHocPoids().contains("6"));
//    }

//    @Test
//    public void createInvoice() {
//        String EQUAL = "=";
//        String[] segments = {"", "createInvoices = true", "poids-1-2-3"};
//        boolean createInvoices = Boolean.valueOf(segments[1].trim().split(EQUAL)[1]);
//        if (createInvoices) {
//            String poids = segments[2].trim().split(EQUAL)[1];
//            String[] poidArr = poids.split("-");
//            Consumer consumer = new Consumer();
//            consumer.setCreateInvoices(createInvoices);
//            consumer.setAdhocPoids(new HashSet<>());
//            ;
//            for (String poid : poidArr) {
//                consumer.getAdhocPoids().add(poid);
//            }
//            assertEquals(3, consumer.getAdhocPoids().size());
//            verify(consumer).setCreateInvoices(true);
//            verify(consumer).setAdhocPoids(new HashSet<>(Arrays.asList("1", "2", "3")));
//            Span s = Span. current () ;
//            toLog.forEach((k,v) -> {
//                if((v != "") && StringUtils.isNumeric(String.valueOf(v))){
//                    s.setAttribute(k, (long)v);
//                } else{
//                    s.setAttribute(k, String.valueOf(v));
//                }
//            });
//        }
//    }



//    @Test
//    void testMutationCoverage() {
//        // Test case for mutation coverage
//        Map<String, Object> toLog = new HashMap<>();
//        toLog.put("key1", "value1");
//        toLog.put("key2", 123);
//
//        // Test with non-numeric value
//        Span s = Span.current();
//        s.setAttribute("key1", 0L);
//        s.setAttribute("key2", "");
//        toLog.forEach((k, v) -> {
//            if ((v != null) && StringUtils.isNumeric(String.valueOf(v))) {
//                s.setAttribute(k, (long) v);
//            } else {
//                s.setAttribute(k, String.valueOf(v));
//            }
//        });
//        assertEquals("value1", s.getAttribute("key1"));
//        assertEquals("123", s.getAttribute("key2"));
//
//        // Test with null value
//        toLog.put("key3", null);
//        s.setAttribute("key3", "default");
//        toLog.forEach((k, v) -> {
//            if ((v != null) && StringUtils.isNumeric(String.valueOf(v))) {
//                s.setAttribute(k, (long) v);
//            } else {
//                s.setAttribute(k, String.valueOf(v));
//            }
//        });
//        assertEquals("value1", s.getAttribute("key1"));
//        assertEquals("123", s.getAttribute("key2"));
//        assertEquals("default", s.getAttribute("key3"));
//    }

}
