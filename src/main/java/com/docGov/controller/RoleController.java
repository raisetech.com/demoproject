package com.docGov.controller;

import com.docGov.dto.ResponseType;
import com.docGov.dto.RoleDTO;
import com.docGov.models.Role;
import com.docGov.models.User;
import com.docGov.service.RoleService;
import com.docGov.service.UserService;
import com.docGov.utils.GlobalKeyConstants;
import com.docGov.utils.ResponseBuilder;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("role")
@Api(value = "Role", tags = {"Role"})
public class RoleController {

    private final UserService userService;
    private final RoleService roleService;
    boolean roleExist;


    public RoleController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @PostMapping(value = "/{organisationId}/{userId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> createRole(@PathVariable("organisationId") Long organisationId,
                                          @RequestBody RoleDTO roleDTO, @PathVariable("userId") Long userId) {
        try {
           User  userExist =checkUser(userId);
            if (userExist==null) {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "User not found");
            }

            return ResponseBuilder.buildObjectResponse(roleService.createRole(organisationId,
                    roleDTO,userId), ResponseType.SUCCESS, "User created successfully");
        } catch (Exception e) {
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, e.getMessage());
        }

    }

    @GetMapping(value = "", produces = "application/json")
    public Map<String, Object> userList() {
        try {
            return ResponseBuilder.buildListResponse(roleService.findAllRole().stream().collect(Collectors.toList()), ResponseType.SUCCESS, "List of Users");
        } catch (Exception ex) {
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, ex.getMessage());
        }
    }


    @PutMapping(value = "/{organisationId}/{roleId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> updateRole(@PathVariable("organisationId") Long organisationId,@PathVariable("roleId") Long roleId, @RequestBody RoleDTO roleDTO) {
        try {


            Role role = roleService.findRole(roleId);
            if (role == null) {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Role not found");
            }
            roleExist = roleService.roleNameExistForUpdate(roleDTO.getName(),roleId,organisationId);

            if (roleExist) {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Role name already exist");
            }

            return ResponseBuilder.buildObjectResponse(roleService.updateRole(roleId, roleDTO), ResponseType.SUCCESS, "User Updated successfully");
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseBuilder.buildObjectResponse("null", ResponseType.ERROR, ex.getMessage());
        }
    }

    @DeleteMapping(value = "/{roleId}/{userId}/{deleteReason}", produces = "application/json")
    public Map<String, Object> deleteRole(@PathVariable("roleId") Long roleId,
                                         @PathVariable("userId") Long userId,
                                          @PathVariable("deleteReason") String deleteReason) {
        try {
            User user = checkUser(userId);

            if(user==null){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "User not found!");
            }

            if(deleteReason==null || deleteReason.trim().isEmpty()){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Delete reason must not be empty!");
            }

            Role role = roleService.findRole(roleId);
            if (role != null) {
                return ResponseBuilder.buildObjectResponse(roleService.deleteRole(roleId,userId,deleteReason),
                        ResponseType.SUCCESS, "Role deleted Successfully.");
            } else {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Role not found!");
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, ex.getMessage());
        }
    }
    @GetMapping(value = "/moduleList", produces = "application/json")
    public  Map<String,Object> getRoleModules(){
        return ResponseBuilder.buildListResponse(roleService.roleModuleList().stream().
                collect(Collectors.toList()), ResponseType.SUCCESS, GlobalKeyConstants.listFound);
    }


    public User checkUser(long userId) {

        return userService.findUser(userId);
    }
}
