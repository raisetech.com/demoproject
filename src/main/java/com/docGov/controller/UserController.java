package com.docGov.controller;

import com.docGov.dto.PasswordChangeDTO;
import com.docGov.dto.ResponseType;
import com.docGov.dto.UserDTO;
import com.docGov.exceptionHandler.GlobalException;
import com.docGov.models.User;
import com.docGov.service.UserService;
import com.docGov.utils.GlobalKeyConstants;
import com.docGov.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("users")
public class UserController {


    private final UserService service;
    boolean userExist;
    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping(value = "/{userId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> createUser(@PathVariable("userId") Long userId,
                                          @RequestBody UserDTO userDTO) {
        try {

            userExist = service.usernameExist(userDTO.getUsername());
            if (!userExist) {
                return ResponseBuilder.buildObjectResponse(service.createUser(
                        userDTO,false,userId,0), ResponseType.SUCCESS, "User created successfully");
            } else {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Username already exist");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, e.getMessage());
        }




    }
    @PostMapping(value = "/saveUser/{userId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> saveUser(@PathVariable("userId") Long userId,
                                                      @RequestBody UserDTO userDTO) {
        try {

            userExist = service.usernameExist(userDTO.getUsername());
            if (!userExist) {
                return ResponseBuilder.buildObjectResponse(service.createUser(
                        userDTO, false, userId, 0).getId(), ResponseType.SUCCESS, "User created successfully");
            } else {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Username already exist");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, e.getMessage());
        }
    }

    @GetMapping(value = "/userList", produces = "application/json")
    public Map<String, Object> userList() {
        try {
            return ResponseBuilder.buildListResponse(service.findAllUser().stream().collect(Collectors.toList()),
                    ResponseType.SUCCESS, "List of Users");
        } catch (Exception ex) {
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, ex.getMessage());
        }
    }



    @PutMapping(value = "/{userId}/{updatedId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> updateUser(@PathVariable("userId") Long userId,
                                          @RequestBody UserDTO userDTO,
                                          @PathVariable("updatedId") Long updatedId) {
        try {
            User user = service.findUser(userId);
            if (user == null) {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "User not found");
            }
            userExist = service.usernameExist(userDTO.getUsername());
            if (userExist) {
                return ResponseBuilder.buildObjectResponse(service.updateUser(userId, userDTO,0), ResponseType.SUCCESS, "User Updated successfully");
            } else {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Username already exist");
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildObjectResponse("null", ResponseType.ERROR, ex.getMessage());
        }
    }
    @PutMapping(value = "/resetPassword/{userId}", produces = "application/json", consumes = "application/json")
    public Map<String, Object> resetPassword(@PathVariable("userId") Long userId, @RequestBody PasswordChangeDTO changeDTO) {
        try {
            User user = service.findUser(userId);
            if (user == null) {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "User not found");
            }

            if(changeDTO.getOldPassword()==null|| changeDTO.getOldPassword().isEmpty()){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "Old password cannot be empty!");
            }
            if(changeDTO.getNewPassword()==null|| changeDTO.getNewPassword().isEmpty()){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "New password cannot be empty!");
            }
            service.resetPassword(user,changeDTO);
            return ResponseBuilder.buildNullResponse("", ResponseType.SUCCESS, "Password updated successfully");
        } catch (GlobalException ex) {
            return ResponseBuilder.buildObjectResponse("null", ResponseType.ERROR, ex.getMessage());
        }
    }

    @DeleteMapping(value = "/{userId}/{deletedById}/{deleteReason}", produces = "application/json")
    public Map<String, Object> deleteUser(@PathVariable("userId") Long userId,
                                          @PathVariable("deletedById") Long deletedById,
                                          @PathVariable("deleteReason") String deleteReason) {
        try {
            User deletedBy =service.findUser(deletedById);
            if(deletedBy==null){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR,
                        "User not found!");
            }

            if(userId.equals(deletedById) ) {
                return  ResponseBuilder.buildObjectResponse(null,ResponseType.ERROR, "Cannot delete loggedIn User.");
            }
            if(deleteReason==null || deleteReason.trim().isEmpty()){
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR,
                        "Reason must not be empty!");
            }

            User user = service.findUser(userId);
            if (user != null) {
                return ResponseBuilder.buildObjectResponse(service.deleteUser(user,deletedById,deleteReason),
                        ResponseType.SUCCESS, "User deleted Successfully.");
            } else {
                return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, "User  not found!");
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildObjectResponse(null, ResponseType.ERROR, ex.getMessage());
        }
    }

    @GetMapping(value = "/userDropDown/{organisationId}", produces = "application/json")
    public Map<String, Object> getuserDropDown(@PathVariable("organisationId") Long organisationId) {
        try {
            return ResponseBuilder.buildListResponse(service.userDropDownList(organisationId).stream().collect(Collectors.toList()),
                    ResponseType.SUCCESS
                    , GlobalKeyConstants.listFound);
        } catch (Exception e) {
            return ResponseBuilder.buildListResponse(null, ResponseType.ERROR, GlobalKeyConstants.listNotFound);
        }
    }

}
