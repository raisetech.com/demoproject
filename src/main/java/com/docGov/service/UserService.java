package com.docGov.service;

import com.docGov.dto.*;
import com.docGov.exceptionHandler.GlobalException;
import com.docGov.mapper.RoleMapper;
import com.docGov.mapper.UserMapper;
import com.docGov.models.Role;
import com.docGov.models.User;
import com.docGov.repository.*;
import com.docGov.utils.Utils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;

    private final UserMapper userMapper;


    private final RoleRepository roleRepository;

//    private FiscalYearRepository fiscalYearRepository;
    private final RoleMapper roleMapper;

    private final Utils utils= new Utils();


    public UserService(UserRepository userRepository, UserMapper userMapper,
                       RoleRepository roleRepository,RoleMapper roleMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.roleRepository = roleRepository;
//        this.fiscalYearRepository = fiscalYearRepository;
        this.roleMapper = roleMapper;
    }

    public List<UserDTO> findAllUser() {
        List<User> userList =userRepository.findAllByOrganisationId();
        List<UserDTO> userDTOList = new ArrayList<>();
        for(User user:userList){
            UserDTO userDTO = new UserDTO();
            userDTO.setRoleName(user.getRoleName());
            if( user.getRoleId()!=null){
                userDTO.setRole(roleMapper.toDTO(user.getRoleId()));
            }
//            if (user.getWarehouseId()!=null) {
//                WarehouseDTO dto = new WarehouseDTO();
//                dto.setId(user.getWarehouseId().getId());
//                dto.setName(user.getWarehouseId().getName());
//                userDTO.setWarehouseId(dto);
//            }
            userDTO=userMapper.toDTO(user);

            userDTOList.add(userDTO);
        }

        return userDTOList;
    }




    public List<UserDTO> findAllAdminUser() {
        return userMapper.toDTOList(userRepository.findAllAdminUser());
    }

    public List<User> findAllUserByOrgId(Long organisationId){
       return userRepository.findAllByOrganisationId();
    }

    public UserDTO createUser(UserDTO userDTO,boolean isAdminUser,long userId,long updatedId) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setPassword(utils.passwordEncoder().encode(userDTO.getPassword()));
        user.setUsername(userDTO.getUsername());
        if(userDTO.getRefUserId()!=0){
            user.setRefUserId(userDTO.getRefUserId());
        }
        if(userId!=0){
            user.setCreatedBy(userId);
        }
        if(updatedId!=0){
            user.setUpdatedBy(updatedId);
        }
        user.setDeleted(false);
        user.setEmail(userDTO.getEmail());
        user.setIsAdmin(isAdminUser);
//        user.setFyId(fiscalYearRepository.currentFiscalyear());
        user.setEnabled(true);
//        if(userDTO.getStatus()==null || userDTO.getStatus().isEmpty()){
//            user.setStatus(StatusEnum.ACTIVE);
//        }else{
//            user.setStatus(StatusEnum.valueOf(userDTO.getStatus()));
//        }
        user.setCreatedDate(new Date());
        Role role= roleRepository.findSuperAdminRole("Super Admin");
        if(role!=null){
            user.setRoleId(role);
        }else{
            Role r = new Role();
            r.setName("Super Admin");
            r.setModules("21.1,21.2");
            r.setDeleted(false);
            roleRepository.save(r);
            user.setRoleId(r);
        }
        if(userDTO.getRole()!=null){
            user.setRoleId(roleRepository.findById(userDTO.getRole().getId()).get());
        }
//        if(userDTO.getFyId()!=null){
//            user.setFyId(fiscalYearRepository.findById(userDTO.getFyId()).get());
//        }
        return userMapper.toDTO(userRepository.save(user));

    }

    public UserDTO deleteUser(User findUser,long userId,String deleteReason) throws GlobalException {

//        if(!findUser.getIsAdmin()) {
//            if (!checkInventoryInExistByUserId(findUser.getId()).isEmpty()) {
//                throw new GlobalException(HttpStatus.BAD_REQUEST, "user found in transaction cannot delete!");
//            }
//            if (!checkInventoryOutExistByUserId(findUser.getId()).isEmpty()) {
//                throw new GlobalException(HttpStatus.BAD_REQUEST, "User found in transaction cannot delete!");
//            }
//            if (!checkTransactionExistByUserId(findUser.getId()).isEmpty()) {
//                throw new GlobalException(HttpStatus.BAD_REQUEST, "User found in transaction cannot delete!");
//            }
//        }


        if (findUser!=null) {
            findUser.setDelDate(new Date());
            findUser.setDeleted(true);
            findUser.setDeleteBy(userId);
            findUser.setDeleteReason(deleteReason);
        }
        return userMapper.toDTO(userRepository.save(findUser));
    }

    public User findUser(Long userId) {
        return userRepository.returnUserByUserId(userId);

    }

    public void resetPassword(User user, PasswordChangeDTO changeDTO) throws GlobalException {
        if(utils.passwordEncoder().matches(changeDTO.getOldPassword(),user.getPassword())){
            user.setPassword(utils.passwordEncoder().encode(changeDTO.getNewPassword()));
            userRepository.save(user);
        }else{
            throw new GlobalException(HttpStatus.BAD_REQUEST, "Old password doesn't matches!");
        }
    }

    public UserDTO updateUser(Long userId, UserDTO userDTO,long updatedBy) {
        User user = findUser(userId);
        User userExisted = user;
        userExisted.setName(userDTO.getName());
        if(updatedBy!=0){
            userExisted.setUpdatedBy(updatedBy);
        }
        if(userDTO.getRole()!=null){
            userExisted.setRoleId(roleRepository.findById(userDTO.getRole().getId()).get());
        }
        userExisted.setUpdatedDate(new Date());
        return userMapper.toDTO(userRepository.save(userExisted));

    }

    public boolean usernameExist(String username) {

        User user = userRepository.findByUsername(username);
        return user != null;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User saveUser(User user){
        return  userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user found for " + username);
        }
        UserDetails details = org.springframework.security.core.userdetails.User.builder().username(username).password(user.getPassword())
                .authorities(Arrays.asList("user").stream().toArray(String[]::new))
                .build();
        return details;
    }

    public List<UserDTO> userDropDownList(Long organisationId){
        List<User> userList = userRepository.findAllByOrganisationId();
        List<UserDTO> userDTOList = new ArrayList<>();
        for(User user: userList){
            UserDTO dto = new UserDTO();
            dto.setName(user.getName());
            dto.setId(user.getId());
            userDTOList.add(dto);
        }
        return userDTOList;
    }

}
