package com.docGov.service;

import com.docGov.dto.CommonDTO;
import com.docGov.dto.ModulePackageDTO;
import com.docGov.mapper.ModulePackageMapper;
import com.docGov.models.ModulePackage;
import com.docGov.repository.ModulePackageRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModulePackageService {

private final ModulePackageRepository modulePackageRepository;
private ModulePackageMapper modulePackageMapper;

    public ModulePackageService(ModulePackageRepository modulePackageRepository) {
        this.modulePackageRepository = modulePackageRepository;
    }

    public List<CommonDTO> returnModulePackages(){
        List<CommonDTO> commonDTOList = new ArrayList<>();
        List<ModulePackage> modulePackageList = modulePackageRepository.findAll();
        for(ModulePackage modulePackage:modulePackageList){
            CommonDTO commonDTO = new CommonDTO();
            commonDTO.setId(modulePackage.getId());
            commonDTO.setName(modulePackage.getName());
            commonDTOList.add(commonDTO);
        }
        return commonDTOList;
    }

    private ModulePackageDTO addModulePackage(ModulePackageDTO modulePackageDTO){
        ModulePackage modulePackage = new ModulePackage();
        modulePackage.setModuleContains(modulePackageDTO.getModuleContains());
        modulePackage.setModulesEnums(modulePackageDTO.getModulesEnums());
        modulePackage.setName(modulePackageDTO.getName());
        return modulePackageMapper.toDTO(modulePackageRepository.save(modulePackage));
    }

}
