package com.docGov.service;

import com.docGov.dto.ModuleDTO;
import com.docGov.dto.RoleDTO;
import com.docGov.exceptionHandler.GlobalException;
import com.docGov.mapper.RoleMapper;
import com.docGov.models.ModulePackage;
import com.docGov.models.Role;
import com.docGov.repository.ModulePackageRepository;
import com.docGov.repository.RoleRepository;
import com.docGov.utils.PageEnum;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RoleService {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;
    private final UserService userService;
    private final ModulePackageRepository modulePackageRepository;
    String roles = "";

    public RoleService(RoleRepository roleRepository, RoleMapper roleMapper,
                       UserService userService,
                       ModulePackageRepository modulePackageRepository) {
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
        this.userService = userService;
        this.modulePackageRepository = modulePackageRepository;
    }

    public List<RoleDTO> findAllRole() {
        return roleMapper.toDTOList(roleRepository.findAllByOrganisationId());
    }


    public RoleDTO createRole(Long organisationId, RoleDTO roleDTO, Long userId) {
        Role role = new Role();
        roles = "";
        for (ModuleDTO moduleDTO : roleDTO.getModuleList()) {

            if (moduleDTO.getRoleId() != null) {
                roles = roles + moduleDTO.getRoleId();
            }

        }
        role.setModules(roles);
        role.setDeleted(false);
        role.setCreatedDate(new Date());
        role.setName(roleDTO.getName());
        role.setUserId(userService.findUser(userId));
        return roleMapper.toDTO(roleRepository.save(role));

    }

    public RoleDTO deleteRole(long id, long userId, String deleteReason) throws GlobalException {

        if (!checkRoleExistByUserId(id).isEmpty()) {
            throw new GlobalException(HttpStatus.BAD_REQUEST, "Role found in users cannot delete!");
        }

        Role findRole = findRole(id);
        findRole.setUpdatedDate(new Date());
        findRole.setDeleted(true);
        findRole.setDeleteBy(userId);
        findRole.setDeleteReason(deleteReason);
        return roleMapper.toDTO(roleRepository.save(findRole));
    }

    public Role findRole(Long roleId) {
        Role role = roleRepository.findRoleById(roleId);
        return role;
    }

    public RoleDTO updateRole(Long roleId, RoleDTO roleDTO) {
        Role role = findRole(roleId);
        roles = "";
        Role roleExisted = role;
        roleExisted.setModules("");
        System.out.println(roleDTO.getName());
        if (roleDTO.getModuleList()!=null) {
            for (ModuleDTO moduleDTO : roleDTO.getModuleList()) {

                if (moduleDTO.getRoleId() != null) {
                    if (roleExisted.getModules() != null) {
                        roles = roles + moduleDTO.getRoleId();
                    }
                }

            }
        }
        roleExisted.setName(roleDTO.getName());
        roleExisted.setUpdatedDate(new Date());
        roleExisted.setModules(roles);
        return roleMapper.toDTO(roleRepository.save(roleExisted));

    }

    public boolean roleNameExist(String name, long organisationId, long roleId) {

        Role role = roleRepository.findByName(name, roleId);
        return role != null;
    }

    public boolean roleNameExistForUpdate(String name, long roleId,long orgId) {

        Role role = roleRepository.findByNameForUpdate(name, roleId);
        return role != null;
    }

    public List<RoleDTO> roleModuleList() {
        List<RoleDTO> roleDTOS = new ArrayList<>();
        List<String> alreadyDone = new ArrayList<>();
        List<String> moduleList1;
        String modules = "1,";
        ArrayList<String> nodulesArray = new ArrayList<>(Arrays.asList(modules.split(",")));
        String fullPageEnums="";
        for(String a:nodulesArray){
            ModulePackage modulePackage=modulePackageRepository.findById(Long.valueOf(a)).get();
            fullPageEnums=fullPageEnums+modulePackage.getModuleContains();
        }
        String[] array = fullPageEnums.split(",", 0);
        moduleList1 = new ArrayList<>(Arrays.asList(array));
        Set<String> hashmapset = new HashSet<>();
        hashmapset.addAll(moduleList1);
        moduleList1.clear();
        moduleList1.addAll(hashmapset);
        for (PageEnum page1 : PageEnum.values()) {
            RoleDTO role = new RoleDTO();
            List<ModuleDTO> moduleDTOList = new ArrayList<>();
            if (!alreadyDone.contains(page1.getPageHead())) {
                role.setModuleName(page1.getPageHead());
                for (PageEnum page2 : PageEnum.values()) {
                    if (page1.getPageHead().equalsIgnoreCase(page2.getPageHead())) {
                        if (!alreadyDone.contains(page1.getPageHead())) {
                            alreadyDone.add(page1.getPageHead());
                        }
                        for (String m : moduleList1) {
                            ModuleDTO module = new ModuleDTO();
                            module.setRoleId(String.valueOf(page2.getPageId()));
                            module.setRoleName(page2.getPageName());
                            if (m.equalsIgnoreCase(String.valueOf(page2.getPageId()))) {
                                moduleDTOList.add(module);
                                break;
                            }
                        }
                    }
                }
                role.setModuleList(moduleDTOList);
                roleDTOS.add(role);
            }
        }
        return roleDTOS;
    }

    public List<Object[]> checkRoleExistByUserId(long userId) {
        return roleRepository.checkRoleExistByUserId(userId);
    }
}
