package com.docGov.models;


public enum RoleEnum {


    //Accounts//
    AccountHead(1.01, "Master", "Account", "Account Head"),
    AccountSubHead(1.02, "Master", "Account", "Account Sub Head"),
    AccountMaster(1.03, "Master", "Account", "Account Master"),
    CustomerAccount(1.04, "Master", "Account", "Customer Account"),
    SupplierAccount(1.05, "Master", "Account", "Supplier Account"),

    AccountAdd(1.05, "Master", "Account", "Add"),
    AccountUpdate(1.07, "Master", "Account", "Update"),
    AccountDelete(1.08, "Master", "Account", "Delete"),


    //Inventory
    ItemGroup(1.09, "Master", "Inventory", "Item Group"),
    ItemSubGroup(1.11, "Master", "Inventory", "Item Sub Group"),
    ItemMaster(1.12, "Master", "Inventory", "Item Master"),
    MeasurementUnit(1.13, "Master", "Inventory", "Measurement Unit"),

    InventoryAdd(1.15, "Master", "Inventory", "Add"),
    InventoryUpdate(1.14, "Master", "Inventory", "Update"),
    InventoryDelete(1.16, "Master", "Inventory", "Delete"),

    Warehouse(1.17, "Master", "Warehouse", "Warehouse"),

    WarehouseAdd(1.18, "Master", "Warehouse", "Add"),
    WarehouseUpdate(1.19, "Master", "Warehouse", "Update"),
    WarehouseDelete(1.21, "Master", "Warehouse", "Delete"),

    ExtraExpensesPurchase(1.22, "Master", "Bill Sundry", "Extra Expenses Purchase"),
    ExtraExpensesSales(1.23, "Master", "Bill Sundry", "Extra Expenses Sales"),

    BillSundryAdd(1.24, "Master", "Warehouse", "Add"),
    BillSundryUpdate(1.25, "Master", "Warehouse", "Update"),
    BillSundryDelete(1.26, "Master", "Warehouse", "Delete"),


    JournalEntry(2.01, "Transaction", "Accounting", "Journal"),
    ReceiptEntry(2.02, "Transaction", "Accounting", "Receipt"),
    PaymentEntry(2.03, "Transaction", "Accounting", "Payment"),
    DebitNote(2.04, "Transaction", "Accounting", "Debit Note"),
    CreditNote(2.05, "Transaction", "Accounting", "Credit Note"),

    AccountingAdd(2.06, "Transaction", "Accounting", "Add"),
    AccountingUpdate(2.07, "Transaction", "Accounting", "Update"),
    AccountingDelete(2.08, "Transaction", "Accounting", "Delete"),

    SalesOrder(2.09, "Transaction", "Sales", "Sales Order"),
    SalesChallan(2.11, "Transaction", "Sales", "Sales Challan"),
    SalesInvoice(2.12, "Transaction", "Sales", "Sales Invoice"),

    SalesAdd(2.13, "Transaction", "Sales", "Add"),
    SalesUpdate(2.14, "Transaction", "Sales", "Update"),
    SalesDelete(2.15, "Transaction", "Sales", "Delete"),

    PurchaseOrder(2.16, "Transaction", "Purchase", "Purchase Order"),
    PurchaseChallan(2.17, "Transaction", "Purchase", "Purchase Challan"),
    PurchaseInvoice(2.18, "Transaction", "Purchase", "Purchase Invoice"),

    PurchaseAdd(2.19, "Transaction", "Purchase", "Add"),
    PurchaseUpdate(2.21, "Transaction", "Purchase", "Update"),
    PurchaseDelete(2.22, "Transaction", "Purchase", "Delete"),

    Contra(2.23, "Transaction", "Banking", "Contra"),
    CashWithdraw(2.24, "Transaction", "Banking", "Cash Withdraw"),
    CashDeposit(2.25, "Transaction", "Banking", "Cash Deposit"),
    BankTransfer(2.26, "Transaction", "Banking", "Bank Transfer"),

    BankingAdd(2.27, "Transaction", "Banking", "Add"),
    BankingUpdate(2.28, "Transaction", "Banking", "Update"),
    BankingDelete(2.29, "Transaction", "Banking", "Delete"),


    CashBook(3.01, "Accounting Report", "Account Book", "Cash Book"),
    DayBook(3.02, "Accounting Report", "Account Book", "Day Book"),
    BankDetails(3.03, "Accounting Report", "Account Book", "Bank Details"),
    JournalBook(3.04, "Accounting Report", "Account Book", "Journal Book"),
    AccountLedger(3.05, "Accounting Report", "Account Book", "Account Ledger"),

    AccountReceivable(3.06, "Accounting Report", "Outstanding Report", "Account Receivable"),
    AccountPayment(3.07, "Accounting Report", "Outstanding Report", "Account Payable"),

    OpeningTrialBalanceAccountHeadWise(3.08, "Accounting Report", "Final Reports",
            "Opening Trial Balance - Account Head wise"),
    ClosingTrialBalanceAccountHeadWise(3.09, "Accounting Report", "Final Reports",
            "Closing Trial Balance - Account Head wise"),

    OpeningTrialBalanceDetailedAccountHeadWise(3.11, "Accounting Report", "Final Reports",
            "Opening Trial Balance - Account Head wise"),
    ClosingTrialBalanceDetailedAccountHeadWise(3.12, "Accounting Report", "Final Reports",
            "Closing Trial Balance - Account Head wise"),



    StockLedger(4.01, "Inventory Report", "Inventory Book", "Stock Ledger"),
    DayBookInventory(4.02, "Inventory Report", "Inventory Book", "Day Book"),
    ClosingStock(4.03, "Inventory Report", "Inventory Book", "Closing Stock"),

    User(5.01, "Set Up", "User", "Manage User"),
  UserAdd(5.02, "Master", "User", "Add"),
    UserUpdate(5.03, "Master", "User", "Update"),
    UserDelete(5.04, "Master", "User", "Delete"),

    Role(5.05, "Set Up", "Role", "Manage Role"),
    RoleAdd(5.06, "Master", "Role", "Add"),
    RoleUpdate(5.07, "Master", "Role", "Update"),
    RoleDelete(5.08, "Master", "Role", "Delete"),



    PastDateEntry(6.01, "Additional Settings", "Transaction", "Past Date Entry"),
    FutureDateEntry(6.02, "Additional Settings", "Transaction", "Future Date Entry");


    RoleEnum(double pageId, String module, String pageHead, String pageName) {
        this.pageId = pageId;
        this.pageHead = pageHead;
        this.pageName = pageName;
        this.module = module;
    }

    private final String pageName;
    private final double pageId;
    private final String pageHead;
    private final String module;

}
