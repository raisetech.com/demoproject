package com.docGov.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "module_package")
@NamedQuery(name = "ModulePackage.findAll", query = "SELECT m FROM ModulePackage m")
public class ModulePackage extends BaseModel<Long>{

    private String name;
    private String moduleContains;
    private String modulesEnums;

    public String getModuleContains() {
        return moduleContains==null?"":moduleContains;
    }
}
