package com.docGov.models;

import javax.persistence.*;

import lombok.*;


/**
 * The persistent class for the roles database table.
 */
@Entity
@Getter
@Setter
@Table(name = "roles")
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role extends BaseModel<Long> {




    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    private String name;

    private String modules;


}