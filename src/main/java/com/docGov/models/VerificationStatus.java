package com.docGov.models;

public enum VerificationStatus {

    pending,forwardedTo,rejected,verified
}
