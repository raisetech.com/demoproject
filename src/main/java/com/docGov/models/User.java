package com.docGov.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * The persistent class for the users database table.
 */
@Getter
@Setter
@Entity
@Table(name = "users")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User extends BaseModel<Long> {

    @Column(name = "confirmation_token")
    private String confirmationToken;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_by")
    private Long updatedBy;


    @Column(name = "CURRENT_CONNECTIONS")
    private Long currentConnections;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "del_date")
    private Date delDate;

    @Column(name = "del_reason")
    private String delReason;


    private String email;

    private String classRoom;

    private String contactNumber;

    private Boolean enabled;


    @Column(name = "is_admin")
    private Boolean isAdmin;

    @Column(name = "is_pos_user")
    private Boolean isPposUser;

    private String name;

    private String wardIds;


    private String password;

    @Column(name = "reset_token")
    private String resetToken;

    @ManyToOne
    @JoinColumn(name = "roleId")
    private Role roleId;

    @Column(name = "TOTAL_CONNECTIONS")
    private Long totalConnections;

    private Boolean ridersAdmin;

    private String username;

    private Long refUserId;

    private Long customerId;

    private Long refOrgId;

    @Transient
    private String roleName;

    public String getRoleName() {
        if (roleId != null) {
            return roleName = roleId.getName();
        }else{
            return roleName = "N/A";
        }
    }


}