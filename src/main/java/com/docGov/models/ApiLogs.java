package com.docGov.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "api_logs")
public class ApiLogs extends BaseModel<Long>{
    private String body;
    private String apiPath;
    private String remarks;
    private String response;
    private String responseCode;

}
