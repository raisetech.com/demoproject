package com.docGov.models;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@MappedSuperclass
@JsonDeserialize
public class BaseModel<T> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private T id;

//    @Column(name = "eanbled", columnDefinition = "bit default 0", nullable = false)
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean deleted;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate ;

//    @ManyToOne
//    @JoinColumn(name = "deleted_by")
    private Long deleteBy;

    private Long enteredBy;

    private String deleteReason;



}
