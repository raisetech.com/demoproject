package com.docGov.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtConfig {

    private String uri = "/auth/**";


    private String header = "Authorization";


    private String prefix = "Bearer ";


    private int expiration = 14400;


    private String secret = "MVRp3LbLtt2udGmG";

}