package com.docGov.securityConfig;


import com.docGov.mapper.UserMapper;
import com.docGov.models.JwtConfig;
//import com.docGov.repository.FiscalYearRepository;
import com.docGov.service.UserService;
import com.docGov.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@EnableWebSecurity
@Configuration
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {


  private final UserService accountService;
  private final UserMapper accountMapper;
//  private FiscalYearRepository fiscalYearRepository;
  private final Utils utils = new Utils();

  public SecurityTokenConfig(UserService accountService, UserMapper accountMapper) {
    this.accountService = accountService;
    this.accountMapper = accountMapper;
//    this.fiscalYearRepository = fiscalYearRepository;
  }

  // CORSfilter
  @Bean
  public CorsFilter corsFilter() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedHeader("*");
    config.addAllowedMethod("OPTIONS");
    config.addAllowedMethod("GET");
    config.addAllowedMethod("POST");
    config.addAllowedMethod("PUT");
    config.addAllowedMethod("DELETE");
    config.addAllowedMethod("get");

    //For production comment this//
        config.addAllowedOrigin("*");
    //For Local
//        config.setAllowedOrigins(Arrays.asList("http://localhost:7080"));

//        config.setAllowedOrigins(Arrays.asList("http://192.168.1.68:7080"));
    //For local use 7080 port
//        config.setAllowedOrigins(Arrays.asList("http://192.168.10.64:7080"));
//        For prdouction//
//    config.setAllowedOrigins(Arrays.asList("http://soilanalysisnepal.com"));

    config.addExposedHeader("Authorization, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Accept, X-Requested-With,Content-Type, Access-Control-Request-Method");

//		config.addAllowedHeader("*");
//		config.a
    source.registerCorsConfiguration("/**", config);
    return new CorsFilter(source);
  }


  @Override
  protected void configure(HttpSecurity http) throws Exception {
    JwtConfig jwtConfig = new JwtConfig();
    log.debug("Config: {}", jwtConfig);
    http
            .csrf().disable()
            .cors().and()
            // make sure we use stateless session; session won't be used to store user's state.
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            // handle an authorized attempts
            .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
            .and()
            .httpBasic().disable()

            // Add a filter to validate the tokens with every request
            .addFilterBefore(new JwtTokenAuthFilter(jwtConfig, accountService, accountMapper), UsernamePasswordAuthenticationFilter.class)
            .addFilter(new JwtCredentialAuthFilter(authenticationManager(), jwtConfig,
                    accountService, accountMapper))
            // authorization requests config

            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers(HttpMethod.GET, "/info").permitAll()
            .antMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()
            .antMatchers(HttpMethod.GET, "/v2/api-docs",
                    "/configuration/ui",
                    "/swagger-resources/**",
                    "/configuration/security",
                    "/webjars/**").permitAll()
            .antMatchers("/ecommerce/**").permitAll()
            .antMatchers("/ecommerce/itemListByCategoryList/**").permitAll()
            .antMatchers("/ecommerce/registerCustomer/**").permitAll()
            .antMatchers("/delivery/dashboardSummary/**").permitAll()
            .antMatchers("/delivery/saveDeliveryRequest/**").permitAll()
            .antMatchers("/delivery/tackDeliveryNoBySender/**").permitAll()
            .antMatchers("/delivery/tackDeliveryNoByReceiver/**").permitAll()
            .antMatchers("/ecommerce/itemCategoryAndSubCategoryList/**").permitAll()

            .antMatchers("/inquiry/saveinquiry/**").permitAll()

            .antMatchers("/item/itemListForSite/**").permitAll()
            .antMatchers("/item/itemListByGroup/**").permitAll()
            .antMatchers("/item/itemMasterById/**").permitAll()

            //This is for thirdPartyApi
            .antMatchers("/users/saveUser/**").permitAll()
            .antMatchers("/transactionMaster/saveSalesVoucher/**").permitAll()
            .antMatchers("/transactionMaster/savePurchaseVoucher/**").permitAll()
            .antMatchers("/transactionMaster/saveReceiptVoucher/**").permitAll()
            .antMatchers("/transactionMaster/saveJournalVoucher/**").permitAll()
            .antMatchers("/transactionMaster/savePaymentVoucher/**").permitAll()
            .antMatchers("/accountMaster/saveCustomerAccount/**").permitAll()
            .antMatchers("/accountMaster/saveSupplierAccount/**").permitAll()
            .antMatchers("/accountMaster/allExpensesMasterList/**").permitAll()
            .antMatchers("/accountMaster/allIncomeMasterList/**").permitAll()
            .antMatchers("/accountMaster/cashAndBankAccount/**").permitAll()

            .antMatchers("/customer/**").permitAll()
            .antMatchers("/**").permitAll()

            .antMatchers("/eventManagement/saveEventManagement/**").permitAll()

            // allow all who are accessing "auth" service
//				.antMatchers(HttpMethod.POST, "/auth/**").permitAll()
            // Any other request must be authenticated
            .anyRequest().authenticated();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(accountService).passwordEncoder(utils.passwordEncoder());
  }
}
