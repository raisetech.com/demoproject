package com.docGov.securityConfig;


import com.docGov.dto.CommonDTO;
import com.docGov.dto.ResponseType;
import com.docGov.dto.UserDTO;
import com.docGov.mapper.UserMapper;
//import com.docGov.models.FiscalYear;
import com.docGov.models.JwtConfig;
//import com.docGov.repository.FiscalYearRepository;
import com.docGov.service.UserService;
import com.docGov.utils.GlobalKeyConstants;
import com.docGov.utils.NepaliDateConverter;
import com.docGov.utils.ResponseBuilder;
import com.docGov.utils.Utils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class JwtCredentialAuthFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtConfig jwtConfig;
    NepaliDateConverter ndc = new NepaliDateConverter();
    // We use auth manager to validate the user credentials
    private final AuthenticationManager authManager;
    private final UserService service;
//    private FiscalYearRepository fiscalYearRepository;
    private final UserMapper mapper;
    Long organisationId, fyId = 0l;
    boolean isEnglishDateEnabled;
    String startDate, endDate, transactionDate, nepaliNewDate, fyName;


    public JwtCredentialAuthFilter(AuthenticationManager authManager, JwtConfig jwtConfig,
                                   UserService service, UserMapper mapper
    ) {
        this.authManager = authManager;
        this.jwtConfig = jwtConfig;
        this.mapper = mapper;
        this.service = service;
//        this.fiscalYearRepository = fiscalYearRepository;


        // By default, UsernamePasswordAuthenticationFilter listens to "/login" path.
        // In our case, we use "/auth". So, we need to override the defaults.
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        try {

            // 1. Get credentials from request
            UserCredentials creds = Utils.getMapper().readValue(request.getInputStream(), UserCredentials.class);

            // 2. Create auth object (contains credentials) which will be used by auth manager
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    creds.getUsername(), creds.getPassword(), Collections.emptyList());


            // 3. Authentication manager authenticate the user, and use UserDetialsServiceImpl::loadUserByUsername() method to load the user.
            return authManager.authenticate(authToken);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // Upon successful authentication, generate a token.
    // The 'auth' passed to successfulAuthentication() is the current authenticated user.
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        Long now = System.currentTimeMillis();
        Date expiry = new Date(now + jwtConfig.getExpiration() * 1000);
        String token = Jwts.builder()
                .setSubject(auth.getName())
                // Convert to list of strings.
                // This is important because it affects the way we get them back in the Gateway.
                .claim("authorities", auth.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .claim("roles", auth.getAuthorities().stream()
                        .filter(e -> e.getAuthority().startsWith("role_"))
                        .map(m -> m.getAuthority().replace("role_", ""))
                        .collect(Collectors.toList()))
                .setIssuedAt(new Date(now))
                .setExpiration(expiry)  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
                .compact();

        // Add token to header

        String packageModule = "";
        response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);
        if (auth.getPrincipal() instanceof User) {
            User profile = (User) auth.getPrincipal();
            com.docGov.models.User user = service.findByUsername(profile.getUsername());
            UserDTO userDTO = mapper.toDTO(user);
//            if (user.getOrganisationId() != null) {
//                organisationId = user.getOrganisationId().getId();
//                isEnglishDateEnabled = user.getOrganisationId().getEnglishDateTransaction();
//                if (user.getOrganisationId().getModulePackage() != null) {
//                }
//            }

            isEnglishDateEnabled=false;
//            FiscalYear fiscalYear = null;
//            if (user.getFyId() != null) {
//                fiscalYear = user.getFyId();
//            } else {
//                fiscalYear = fiscalYearRepository.currentFiscalyear();
//            }
            CommonDTO commonDTO = new CommonDTO();
//            if (fiscalYear != null) {
//                fyId = fiscalYear.getId();
//                fyName = fiscalYear.getName();
//                if (isEnglishDateEnabled) {
//                    startDate = fiscalYear.getStartDate().toString();
//                    endDate = fiscalYear.getEndDate().toString();
//                    transactionDate = fiscalYear.getTransactionStartDate().toString();
//                } else {
//                    startDate = fiscalYear.getStartDateNepali();
//                    endDate = fiscalYear.getEndDateNepali();
//                    transactionDate = fiscalYear.getTransactionDateNepali();
//                    nepaliNewDate = ndc.convertToNepaliDate(new Date());
//                }
//
//                commonDTO.setId(fiscalYear.getId());
//                commonDTO.setName(fiscalYear.getName());
//
//            } else {
                fyId = 0l;

//            }
            Map<String, Object> payload = new HashMap<>();

            payload.put("user", userDTO);
            if(user.getCustomerId()!=null){
                payload.put("customerId", user.getCustomerId());
            }else{
                payload.put("customerId", 0l);
            }
//            if (organisationId != null && user.getOrganisationId() != null) {
                payload.put("organisationId", "1");
                payload.put("organisationName", "Digital Profile");
                payload.put("organisationAddress", "");
                payload.put("organisationContact", "");
                payload.put("organisationPanVatNo", "");
                payload.put("taxType", "dd");

//                if(user.getOrganisationId().getImageId()!=null && !user.getOrganisationId().getImageId().isEmpty()){
//                    payload.put("organisationLogo", user.getOrganisationId().getLogoName());
//                }else{
//                    payload.put("organisationLogo", "http://ecalculo.com/static/img/eCalculo-Logo.png");
//                }

//            }
//            WarehouseDTO warehouseDTO = new WarehouseDTO();
//            if (user.getWarehouseId() != null) {
//                warehouseDTO.setId(user.getWarehouseId().getId());
//                warehouseDTO.setName(user.getWarehouseId().getName());
//                payload.put("warehouseId", warehouseDTO);
//            } else {
//                warehouseDTO.setId(0l);
//                warehouseDTO.setName("");
//                payload.put("warehouseId", warehouseDTO);
//            }

//            CounterDTO counterDTO = new CounterDTO();
//            if (user.getCounterId() != null) {
//                counterDTO.setId(user.getCounterId().getId());
//                counterDTO.setName(user.getCounterId().getName());
//                payload.put("counterId", counterDTO);
//            } else {
//                counterDTO.setId(0l);
//                counterDTO.setName("");
//                payload.put("counterId", counterDTO);
//            }


            payload.put("status", "SUCCESS");
            payload.put("isEnglishDateEnabled", isEnglishDateEnabled);
            payload.put("fyId", fyId.toString());
            payload.put("fiscalYear", commonDTO);
            payload.put("startDate", startDate);
            payload.put("endDate", endDate);
            payload.put("fyName", fyName);
            payload.put("transactionDate", transactionDate);
            payload.put("access_token", token);
            payload.put("expiry", expiry.getTime());
            payload.put("nepaliNewDate", nepaliNewDate);
            payload.put("packageModule", packageModule);


            response.getWriter().write(Utils.getMapper().writeValueAsString(payload));
        } else {
            response.getWriter().write(Utils.getMapper().writeValueAsString(ResponseBuilder.buildObjectResponse
                    (null, ResponseType.ERROR, GlobalKeyConstants.invalidUser)));
        }
        response.setContentType("application/json");
        response.getWriter().flush();
        response.getWriter().close();
//		response.getOutputStream().write();
    }

    // A (temporary) class just to represent the user credentials
    @Data
    private static class UserCredentials {
        private String username, password;
        // getters and setters ...
    }
}

