package com.docGov.securityConfig;


import com.docGov.dto.UserDTO;
import com.docGov.mapper.UserMapper;
import com.docGov.models.JwtConfig;
import com.docGov.models.User;
//import com.docGov.repository.FiscalYearRepository;
import com.docGov.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//import com.eureka.zuul.security.JwtConfig;

public class JwtTokenAuthFilter extends OncePerRequestFilter {

    private final JwtConfig jwtConfig;

    private final UserService service;

    private final UserMapper mapper;
//    private FiscalYearRepository fiscalYearRepository;

    public JwtTokenAuthFilter(JwtConfig jwtConfig, UserService service, UserMapper mapper) {

        this.jwtConfig = jwtConfig;
        this.service = service;
        this.mapper = mapper;
//        this.fiscalYearRepository = fiscalYearRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        // 1. get the authentication header. Tokens are supposed to be passed in the authentication header
        String header = request.getHeader(jwtConfig.getHeader());

        // 2. validate the header and check the prefix
        if (header == null || !header.startsWith(jwtConfig.getPrefix())) {
            chain.doFilter(request, response);        // If not valid, go to the next filter.
            return;
        }

        // If there is no token provided and hence the user won't be authenticated.
        // It's Ok. Maybe the user accessing a public path or asking for a token.

        // All secured paths that needs a token are already defined and secured in config class.
        // And If user tried to access without access token, then he won't be authenticated and an exception will be thrown.

        // 3. Get the token
        String token = header.replace(jwtConfig.getPrefix(), "");


        try {    // exceptions might be thrown in creating the claims if for example the token is expired

            // 4. Validate the token
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtConfig.getSecret().getBytes())
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.getSubject();
            if (username != null) {

                User user = service.findByUsername(username);
                UserDTO dto = null;
                if (user instanceof User) {
                    dto = this.mapper.toDTO(user);
//                    this.service.applyManagerRoles(dto);
                } else {
                    dto = this.mapper.toDTO(user);
                }

                List<String> authorities = Arrays.asList("");
//				List<String> authorities = account.getRoles().stream().map(
//                        e -> e.getPerms().stream().map(p -> p.getName()).toArray(String[]::new)
//                ).flatMap(Arrays::stream).collect(Collectors.toList());

//						(List<String>) claims.get("authorities");

                // 5. Create auth object
                // UsernamePasswordAuthenticationToken: A built-in object, used by spring to represent the current authenticated / being authenticated user.
                // It needs a list of authorities, which has type of GrantedAuthority interface, where SimpleGrantedAuthority is an implementation of that interface
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                        username, null, null);
//				authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList())
                auth.setDetails(dto);
                // 6. Authenticate the user
                // Now, user is authenticated
                SecurityContextHolder.getContext().setAuthentication(auth);
            }

        } catch (Exception e) {
            if (!(e instanceof MalformedJwtException)) {
//                e.printStackTrace();
            }
            // In case of failure. Make sure it's clear; so guarantee user won't be authenticated
            SecurityContextHolder.clearContext();

        }

        // go to the next filter in the filter chain
        chain.doFilter(request, response);
    }

}
