package com.docGov.repository;

import com.docGov.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("select  r from Role  r where r.id=:roleId and r.deleted=false")
    Role findRoleById(@Param("roleId") long roleId);

    @Query("select r from Role r where r.deleted=false")
    List<Role> findAllByOrganisationId();

    @Query("select r from Role r where r.name=:name and r.deleted=false  and r.id <>:roleId")
    Role findByName(@Param("name") String name
            ,@Param("roleId")long roleId);


    @Query("select r from Role r where r.name=:name and r.deleted=false and r.id <>:roleId ")
    Role findByNameForUpdate(@Param("name") String name,@Param("roleId") long roleId);

    @Query("select r from Role r where r.name=:name and r.deleted=false ")
    Role findSuperAdminRole(@Param("name") String name);

    @Query(value = "SELECT r.id FROM roles r, users u WHERE  u.`role_id`=r.`id` AND r.`id`=:roleId  AND r.`deleted`=FALSE LIMIT 1",nativeQuery = true)
    List<Object[]> checkRoleExistByUserId(@Param("roleId") long roleId);

}
