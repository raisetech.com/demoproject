package com.docGov.repository;

import com.docGov.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Query("select u from User u where  u.deleted=false ")
    List<User> findAllByOrganisationId();


    @Query("select u from User u where u.deleted=false and u.isAdmin=true")
    List<User> findAllAdminUser();


    @Query("select u from User u where u.username=:username and u.deleted=false")
    User findByUsername(@Param("username") String username);

    @Query("select u from User u where u.id=:userId and u.deleted=false")
    User  returnUserByUserId(@Param("userId") Long userId);


}
