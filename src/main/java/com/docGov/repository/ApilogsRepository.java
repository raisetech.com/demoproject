package com.docGov.repository;

import com.docGov.models.ApiLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApilogsRepository extends JpaRepository<ApiLogs, Long> {

}
