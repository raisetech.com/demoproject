package com.docGov.repository;

import com.docGov.models.ModulePackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModulePackageRepository extends JpaRepository<ModulePackage, Long> {

    @Query("select t from ModulePackage t where t.deleted=false  and t.modulesEnums=:moduleEnums" )
    List<ModulePackage> findModulePackageByEnums(@Param("moduleEnums") String moduleEnums);
}
