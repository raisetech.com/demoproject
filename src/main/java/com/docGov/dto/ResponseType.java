package com.docGov.dto;

public enum ResponseType {
    SUCCESS,
    ERROR
}
