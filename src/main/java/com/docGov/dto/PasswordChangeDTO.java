package com.docGov.dto;

import lombok.Data;

@Data
public class PasswordChangeDTO {
    String oldPassword,newPassword;
}
