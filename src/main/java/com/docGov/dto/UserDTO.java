package com.docGov.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO extends BaseDTO {
    private String username, password, name, createdBy, email, delReason, delDate,roleName,status,contactNumber;
    private Long roleId,fyId,warehouse,counter;
    private long refUserId;
    private long refOrgId,customerId;
    private RoleDTO role;
    private boolean ridersAdmin;
    private String wardIds;
//    private CommonDTO roleI;
}
