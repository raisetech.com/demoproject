package com.docGov.dto;

import com.docGov.models.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiLogDTO extends BaseModel<Long> {
    private String body;
    private String apiPath;
    private String remarks;
    private String response;
    private String responseCode;
}
