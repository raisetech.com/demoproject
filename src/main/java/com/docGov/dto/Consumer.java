package com.docGov.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

//@Getter
//@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Consumer {
    public Boolean createInvoices;

    private Set<Object> adhocPoids;

    public Set<Object> getAdhocPoids() {
        return adhocPoids;
    }

    public void setAdhocPoids(Set<Object> adhocPoids) {
        this.adhocPoids = adhocPoids;
    }

    public Boolean getCreateInvoices() {
        return createInvoices;
    }

    public void setCreateInvoices(Boolean createInvoices) {
        this.createInvoices = createInvoices;
    }
}
