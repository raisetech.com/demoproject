package com.docGov.utils;

import lombok.Getter;

@Getter
public enum PageEnum {


    User("User", 5.01,"Setting"),
    Role("Role", 5.02,"Setting");



    PageEnum(String pageName, double pageId, String pageHead) {
        this.pageName = pageName;
        this.pageId = pageId;
        this.pageHead = pageHead;
    }

    private final String pageName;
    private final double pageId;
    private final String pageHead;
}
