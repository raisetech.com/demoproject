package com.docGov.utils;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.util.StringUtils;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Slf4j
public class Utils {
    private static ObjectMapper mapper;


    /**
     * Checks for the key and also check if value is not empty string
     *
     * @param key
     * @param haystack
     * @return
     */
    public static boolean hasKey(String key, Map haystack) {
        return haystack.containsKey(key) && !haystack.get(key).toString().isEmpty();
    }

    public static ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.registerModule(new JodaModule());
            mapper.registerModule(new JavaTimeModule());
            mapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper;
        }
        return mapper;
    }

    public static List jsonToList(String json) throws IOException {
        return mapper.readValue(json, List.class);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return  new BCryptPasswordEncoder();
    }

    public static Date formatYYYMMDDDate(Date d) {
        return parseFormatParse(d, "yyyy-MM-dd");
    }
    public static Date parseFormatParse(Date fecha, String pattern) {
        if (fecha == null) {
            return null;
        } else {
            try {
                SimpleDateFormat formater = new SimpleDateFormat(pattern);
                return formater.parse(formater.format(fecha));
            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    public static Date parseDateYYYMMDD(String d) {
        return parseDate(d, "yyyy-MM-dd");
    }

    public static Date parseDate(String fecha, String pattern) {
        if (StringUtils.isEmpty(fecha)) {
            return null;
        } else {
            try {
                return new SimpleDateFormat(pattern).parse(fecha);
            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static Object[] checkDateForReport(String fromDate, String toDate){
        NepaliDateConverter ndc= new NepaliDateConverter();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Object[] obj = new Object[2];
        Date startDate=ndc.convertToEnglishDate(fromDate);
        obj[0]=sdf.format(startDate);
        Date endDate=ndc.convertToEnglishDate(toDate);
        obj[1]=sdf.format(endDate);
        return obj;
    }

    public static Date returnEnglishDateFromNepaliDateForEntry(Date purSalesDate){
        NepaliDateConverter ndc= new NepaliDateConverter();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        String date=sdf.format(purSalesDate);
        System.out.println("date"+date);
        return ndc.convertToEnglishDate(date);
    }

    public static String returnNepaliDateFromStringEnglishDate(String englishDate) throws ParseException {
        NepaliDateConverter ndc= new NepaliDateConverter();
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        Date date= sdf.parse(englishDate);
        return ndc.convertToNepaliDate(date);
    }
    public static boolean numberValue(String cellValue) {
        Pattern pattern = Pattern.compile("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
        Matcher matcher = pattern.matcher(cellValue);
        return matcher.matches();
    }

    public static int getCurrentNepaliYearByFyId(Date date) {
        NepaliDateConverter nd = new NepaliDateConverter();
        return nd.getNepaliYear(date);
    }

////    public static Connection getDbConnection(DatabaseParams params) throws SQLException {
////        Connection connection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%s/%s?nullNamePatternMatchesAll=true"
////                , params.getHost(), params.getPort(), params.getDatabase()), params.getUsername(), params.getPassword());
////        return connection;
////    }
////
////    public static <T extends BaseModel> List setList(List<T> from, List<T> to) {
//////		List<T> output = null;
////        if (to == null) {
////            to = new ArrayList<>();
////        } else {
////            to = to;
////        }
////
////        if (from == null) {
////            return null;
////        }
////
////        List<T> nonExistence = new ArrayList<>();
////        for (final T f : to) {
////            boolean notFound = from != null && !from.stream().filter(f2 -> Objects.nonNull(f.getId())
////                    && f.getId().equals(f2.getId())).findAny().isPresent();
////            if (notFound) {
////                nonExistence.add(f);
////            }
////        }
////
////
////        List<T> nonDups = new ArrayList<>();
////        for (final T f : from) {
////            boolean notFound = to != null && !to.stream().filter(f2 -> Objects.nonNull(f.getId())
////                    && f.getId().equals(f2.getId())).findAny().isPresent();
////            to.stream().forEach(f2 ->
////            {
////                if (Objects.nonNull(f.getId()) && f.getId().equals(f2.getId())) {
////                    BeanUtils.copyProperties(f, f2);
////                }
////            });
////            if (notFound) {
////                nonDups.add(f);
////            }
////        }
//
////		List<T> duplicates = new ArrayList<>();
////		for(final T f: from){
////			to.stream().forEach(f2 ->
////					{
////					if(Objects.nonNull(f.getId()) && f.getId().equals(f2.getId())) {
////						BeanUtils.copyProperties(f,f2);
////
////					}
////					});
////
////		}
//        //Adding non dupliated partner list
////        to.addAll(duplicates);
//        to.addAll(nonDups);
//        //Removing the non existence parnter list
//        to.removeAll(nonExistence);
//
//        return to;
//    }


}
