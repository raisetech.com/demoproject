package com.docGov.utils;




import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.ArrayList;
import java.util.List;

public class ExcelPrePostProcessUtil {

    public static void addDatatableBorderForExcelFile(Object obj1, Object obj2, int index, boolean lastSummaryIndex) {
        if (obj1 instanceof HSSFSheet) {
            //lastSummaryIndex determines if there is total summary row in the table bottom.
            addDatatableBorderForExcelFileHSSFSheet((HSSFSheet) obj1, (HSSFWorkbook) obj2, index, lastSummaryIndex);
        } else {
            addDatatableBorderForExcelFileXSSFSheet((XSSFSheet) obj1, (XSSFWorkbook) obj2, index, lastSummaryIndex);
        }
    }

    public static void addDatatableBorderForExcelFileHSSFSheet(HSSFSheet sheet, HSSFWorkbook wb, int index, boolean lastSummaryIndex) {

        HSSFCellStyle borderStyleHeaderFooter = HSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_CENTER, XSSFCellStyle.BORDER_THICK);
        HSSFCellStyle borderStyleHeaderFooterRight = HSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_RIGHT, XSSFCellStyle.BORDER_THICK);

        List<String> leftRightAligns = new ArrayList<>();
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_RIGHT);

        HSSFCellStyle borderStyleBody = HSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns);
        HSSFCellStyle borderStyleBodyRight = HSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns, GlobalKeyConstants.ALIGNMENT_RIGHT);

        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_DOWN);
        HSSFCellStyle lastSummary = HSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns);
        HSSFCellStyle lastSummaryRight = HSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns, GlobalKeyConstants.ALIGNMENT_RIGHT);

        int k = sheet.getFirstRowNum() + index;
        int cellCount = 0;
        if (sheet.getRow(k) == null) {
            return;
        }
        for (int l = 0; l <= sheet.getRow(k).getPhysicalNumberOfCells(); l++) {
            if (sheet.getRow(k).getCell(l) != null) {
                cellCount++;
            }
//            System.out.println(cellCount + " - " + sheet.getRow(k).getCell(l));
        }

        List<Integer> boldRowIndex = new ArrayList<>();
        if (index > 0) {
            for (int m = 0; m < 2; m++) {
                boldRowIndex.add(m);
            }
        } else {
            boldRowIndex.add(0);
        }
        //this is for which having Total in table
        int lastRow = -1;
        int n = k * 1;

        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            HSSFRow row = sheet.getRow(n - index);
            if (row != null) {
                lastRow++;
            }
            n++;
        }
        boldRowIndex.add(lastRow);
//        System.out.println("boldRowIndex " + boldRowIndex);

        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            HSSFRow row = sheet.getRow(k - index);
            if (row != null) {
                for (int l = 0; l < cellCount; l++) {

                    if (row.getCell(l) == null) {
                        row.createCell(l).setCellValue("");
                    }
                    if (boldRowIndex.contains(j)) {
                        if (lastSummaryIndex) {
                            if (Utils.numberValue(row.getCell(l).toString())) {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooterRight);
                            } else {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooter);
                            }
                        } else {
                            if (j == lastRow) {
                                if (Utils.numberValue(row.getCell(l).toString())) {
                                    row.getCell(l).setCellStyle(lastSummaryRight);
                                } else {
                                    row.getCell(l).setCellStyle(lastSummary);
                                }
                            } else {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooter);
                            }
                        }
                    } else {
                        if (Utils.numberValue(row.getCell(l).toString())) {
                            row.getCell(l).setCellStyle(borderStyleBodyRight);
                        } else {
                            row.getCell(l).setCellStyle(borderStyleBody);
                        }

                    }
//                    System.out.println(row.getCell(l).getCellType() + " - " + j + " - " + l + " - " + row.getCell(l));
                }
            }
            k++;
        }
    }

    public static void addDatatableBorderForExcelFileXSSFSheet(XSSFSheet sheet, XSSFWorkbook wb, int index, boolean lastSummaryIndex) {

        XSSFCellStyle borderStyleHeaderFooter = XSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_CENTER, XSSFCellStyle.BORDER_THICK);
        XSSFCellStyle borderStyleHeaderFooterRight = XSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_RIGHT, XSSFCellStyle.BORDER_THICK);

        List<String> leftRightAligns = new ArrayList<>();
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_RIGHT);

        XSSFCellStyle borderStyleBody = XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns);
        XSSFCellStyle borderStyleLeftAlign = XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns, GlobalKeyConstants.ALIGNMENT_RIGHT);

        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_DOWN);
        XSSFCellStyle lastSummary = XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns);
        XSSFCellStyle lastSummaryRight = XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns, GlobalKeyConstants.ALIGNMENT_RIGHT);

        int k = sheet.getFirstRowNum() + index;
        int cellCount = 0;
        if (sheet.getRow(k) == null) {
            return;
        }
        for (int l = 0; l <= sheet.getRow(k).getPhysicalNumberOfCells(); l++) {
            if (sheet.getRow(k).getCell(l) != null) {
                cellCount++;
            }
//            System.out.println(cellCount + " - " + sheet.getRow(k).getCell(l));
        }

        List<Integer> boldRowIndex = new ArrayList<>();
        if (index > 0) {
            for (int m = 0; m < 2; m++) {
                boldRowIndex.add(m);
            }
        } else {
            boldRowIndex.add(0);
        }
        int lastRow = -1;
        int n = k * 1;

        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            XSSFRow row = sheet.getRow(n - index);
            if (row != null) {
                lastRow++;
            }
            n++;
        }
        boldRowIndex.add(lastRow);
//        System.out.println("boldRowIndex " + boldRowIndex);

        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            XSSFRow row = sheet.getRow(k - index);
            if (row != null) {
                for (int l = 0; l < cellCount; l++) {

                    if (row.getCell(l) == null) {
                        row.createCell(l).setCellValue("");
                    }
                    if (boldRowIndex.contains(j)) {
                        if (lastSummaryIndex) {
                            if (Utils.numberValue(row.getCell(l).toString())) {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooterRight);
                            } else {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooter);
                            }
                        } else {
                            if (j == lastRow) {
                                if (Utils.numberValue(row.getCell(l).toString())) {
                                    row.getCell(l).setCellStyle(lastSummaryRight);
                                } else {
                                    row.getCell(l).setCellStyle(lastSummary);
                                }
                            } else {
                                row.getCell(l).setCellStyle(borderStyleHeaderFooter);
                            }
                        }
                    } else {
                        if (Utils.numberValue(row.getCell(l).toString())) {
                            row.getCell(l).setCellStyle(borderStyleLeftAlign);
                        } else {
                            row.getCell(l).setCellStyle(borderStyleBody);
                        }
                    }
//                    System.out.println(j + " - " + l + " - " + row.getCell(l));
                }
            }
            k++;
        }
    }

    public static void addDatatableBorderForExcelFileForClosing(XSSFSheet sheet, XSSFWorkbook wb,int k) {
        List<String> leftRightAligns = new ArrayList<>();
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_RIGHT);
        int index = 2;
        int kl = sheet.getFirstRowNum() + index;
        int cellCount = 0;
        if (sheet.getRow(kl) == null) {
            return;
        }
        for (int l = 0; l <= sheet.getRow(kl).getPhysicalNumberOfCells(); l++) {
            if (sheet.getRow(kl).getCell(l) != null) {
                cellCount++;
            }
//            System.out.println(cellCount + " - " + sheet.getRow(k).getCell(l));
        }

        List<Integer> boldRowIndex = new ArrayList<>();
        List<Integer> boldRowIndex1 = new ArrayList<>();
        if (index > 0) {
            for (int m = 0; m < 2; m++) {
                boldRowIndex.add(m);
            }
        } else {
            boldRowIndex.add(0);
        }
        int lastRow = -1;
        int n = k * 1;
        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            XSSFRow row = sheet.getRow(n - index);
            if (row != null) {
                lastRow++;
            }
            n++;
        }
        boldRowIndex.add(lastRow);

        for (int j = 0; j < sheet.getPhysicalNumberOfRows(); j++) {
            XSSFRow row = sheet.getRow(k);
            if (row != null) {
                for (int l = 0; l < 10; l++) {

                    if (row.getCell(l) == null) {
                        row.createCell(l).setCellValue("");
                    }

                    //this  is for the index to find where closing balance is and see in k value
                    if (row.getCell(l).toString().contains("Closing Balance")) {
                        boldRowIndex1.add(row.getRowNum());
                    }
                    if (k == 1 || k == 2 || boldRowIndex1.contains(k)) {
                        row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_CENTER, XSSFCellStyle.BORDER_THICK));
                    } else {
                        boldRowIndex1 = new ArrayList<>();
                        if (row.getCell(l).toString().equals("DR")) {
                            row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyle(wb, GlobalKeyConstants.ALIGNMENT_RIGHT));
                        } else if (row.getCell(l).toString().contains("Transaction of")) {
                            leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_TOP);
                            leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_DOWN);
                            row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyleThickParticular1(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_CENTER, leftRightAligns));
                        } else if (row.getCell(l).toString().contains("Opening Balance")) {
                            row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_LEFT));
                        } else if (row.getCell(l).toString().contains("Closing Balance")) {
                            //this is again coz to make  closing balance only for top alignment
                            boldRowIndex1.add(row.getRowNum());
                        } else if (row.getCell(l).toString().equals("Total :")) {
                            row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyle(wb, Font.BOLDWEIGHT_BOLD, GlobalKeyConstants.ALIGNMENT_LEFT));
                        } else {
                            leftRightAligns.remove(GlobalKeyConstants.ALIGNMENT_TOP);
                            leftRightAligns.remove(GlobalKeyConstants.ALIGNMENT_DOWN);
                            if (Utils.numberValue(row.getCell(l).toString())) {
                                row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns, GlobalKeyConstants.ALIGNMENT_RIGHT));
                            } else {
                                row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns));
                            }

                        }


                    }
//
                    //closing ko lagi
                    for (Integer d : boldRowIndex1) {
                        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_TOP);
                        for (int i = 0; i < 10; i++) {
                            row.getCell(i).setCellStyle(XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns,GlobalKeyConstants.ALIGNMENT_RIGHT));
                        }
                    }
//                    System.out.println("j--" + j + "--" + lastRow);
                    //this is for last row in excel
                    if (j == lastRow + 1) {
                        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_DOWN);
                        leftRightAligns.add(GlobalKeyConstants.ALIGNMENT_TOP);
                        row.getCell(l).setCellStyle(XSSFHelperUtil.getBorderStyleThickParticular(wb, leftRightAligns));
                    }


//                  System.out.println(l + " - " + row.getCell(l));
                }
            }
            k++;
        }


    }
}

