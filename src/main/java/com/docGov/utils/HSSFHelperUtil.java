package com.docGov.utils;



import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

public class HSSFHelperUtil {

    public static HSSFCellStyle getBorderStyle(HSSFWorkbook wb, short fontStyle, String alignment, short borderCellStyle) {
        HSSFCellStyle borderStyle = getBorderStyle(wb, alignment, borderCellStyle);
        Font boldFont = wb.createFont();
        boldFont.setBoldweight(fontStyle);
        borderStyle.setFont(boldFont);
        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyle(HSSFWorkbook wb, short fontStyle, String alignment) {
        HSSFCellStyle borderStyle = getBorderStyle(wb, alignment, HSSFCellStyle.BORDER_THIN);
        Font boldFont = wb.createFont();
        boldFont.setBoldweight(fontStyle);
        borderStyle.setFont(boldFont);
        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyle(HSSFWorkbook wb, String alignment) {
        HSSFCellStyle borderStyle = getBorderStyle(wb, alignment, HSSFCellStyle.BORDER_THIN);
        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyle(HSSFWorkbook wb, String alignment, short borderCellStyle) {

        HSSFCellStyle borderStyle = wb.createCellStyle();

        textAligner(alignment, borderStyle);
        borderStyle(borderStyle, borderCellStyle);

        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyleThickParticular(HSSFWorkbook wb, String thickAlign) {
        HSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAlign);
        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyleThickParticular(HSSFWorkbook wb, List<String> thickAligns) {
        HSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        return borderStyle;
    }

    public static HSSFCellStyle getBorderStyleThickParticular(HSSFWorkbook wb, List<String> thickAligns, String alignment) {
        HSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        textAligner(alignment, borderStyle);
        return borderStyle;
    }

    public static void borderStyleThickParticular(HSSFCellStyle borderStyle, String thickAlign) {

        thickAlignSwitchCase(borderStyle, thickAlign);

        List<String> alignPost = new ArrayList<>();
        alignPost.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_RIGHT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_TOP);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_DOWN);

        for (String str : alignPost) {
            if (!str.equals(thickAlign)) {
                switchCase(borderStyle, str);
            }
        }
    }

    public static void borderStyleThickParticular(HSSFCellStyle borderStyle, List<String> thickAligns) {

        List<String> alignPost = new ArrayList<>();
        alignPost.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_RIGHT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_TOP);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_DOWN);

        for (String str : thickAligns) {

            thickAlignSwitchCase(borderStyle, str);

        }

        for (String str : alignPost) {
            if (!thickAligns.contains(str)) {
                switchCase(borderStyle, str);
            }
        }
    }

    private static void thickAlignSwitchCase(HSSFCellStyle borderStyle, String str) {
        switch (str) {
            case GlobalKeyConstants.ALIGNMENT_LEFT:
                borderStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_RIGHT:
                borderStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_TOP:
                borderStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_DOWN:
                borderStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
                break;
        }
    }

    private static void switchCase(HSSFCellStyle borderStyle, String str) {
        switch (str) {
            case GlobalKeyConstants.ALIGNMENT_LEFT:
                borderStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                break;

            case GlobalKeyConstants.ALIGNMENT_RIGHT:
                borderStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
                break;

            case GlobalKeyConstants.ALIGNMENT_TOP:
                borderStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
                break;

            case GlobalKeyConstants.ALIGNMENT_DOWN:
                borderStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                break;
        }
    }

    public static void borderStyle(HSSFCellStyle borderStyle, short borderCellStyle) {
        borderStyle.setBorderBottom(borderCellStyle);
        borderStyle.setBorderTop(borderCellStyle);
        borderStyle.setBorderRight(borderCellStyle);
        borderStyle.setBorderLeft(borderCellStyle);
    }

    public static void textAligner(String alignment, HSSFCellStyle borderStyle) {
        switch (alignment) {
            case GlobalKeyConstants.ALIGNMENT_CENTER:
                borderStyle.setAlignment(CellStyle.ALIGN_CENTER);
                break;
            case GlobalKeyConstants.ALIGNMENT_LEFT:
                borderStyle.setAlignment(CellStyle.ALIGN_LEFT);
                break;
            case GlobalKeyConstants.ALIGNMENT_RIGHT:
                borderStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                break;
        }
    }
}

