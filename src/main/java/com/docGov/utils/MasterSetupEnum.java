package com.docGov.utils;

public enum MasterSetupEnum {
    BILLBYBILLDETAILS("Account", "Bill By Bill Details", 1.01, "statusDialog", ""),
    CREDITLIMITS("Account", "Credit Limits", 1.02, "statusDialog", ""),
    //    TARGETS("Account", "Targets", 1.03),
    COSTCENTERS("Account", "Cost Centers", 1.04, "statusDialog", ""),
    //    ACCOUNTWISEINTERESTRATE("Account", "Account Wise Interest Rate", 1.05),
    //    SALESMANBROKERWISEREPORTING("Account", "Salesman/Broker-wise Reporting", 1.06),
    //    BUDGETS("Account", "Budgets", 1.07),
    //    ROYALTYCALCULATION("Account", "Royalty Calculation", 1.08),
    //    COMPANYACTDEPRECIATION("Account", "Company Act's Deprecitation", 1.09),
    //    MAINTAINSUBLEDGER("Account", "Maintain Sub Ledgers", 1.11),
    //    LEDGERRECONCILATION("Account", "Ledger Reconcilation", 1.12),
    MULTICURRENCY("Account", "Multi Currency", 1.13, "statusDialog", ""),
    //    MAINTAINIMAGESNOTESWITHMASTERS("Account", " Maintain Images/Notes with Masters", 1.14),
    //    ENABLEPARTYDASHBOARD("Account", "Enable Party Dashboard", 1.15),
    //        SHOWACCCURBALDURINGVOUCHERENTRY("Account", "Show Accounts Current Balance During Voucher Entry", 1.16),
    //    SINGLEENTRYSYSFORPAYMENTRECEIPTVOUCHERS("Account", "Single Entry System For Payment & Receipt Entry", 1.17),
    //    MAINTAINACCOUNTCATEGORY("Account", "Maintain Account Category", 1.18),
    BANKRECONCILATION("Account", "Bank Reconcilation", 1.19, "statusDialog", ""),
    //    POSTINGINACCTHROUGHSALESANDPURCHASERETURN("Account", "Posting Account Through Sales and Purchase Return", 1.21),
    //    MAINTAINBANKINSTRUMENTDETAILS("Account", "Maintain Bank Instrument Details", 1.22),
    //    CHEQUEPRINTING("Account", "Cheque Printing", 1.23),
    //    POSTDATEDCHEQUESINPAYMENTRECEIPTVOUCHERS("Account", "Post Dated Cheques In Payment/Receipt Vouchers", 1.24),
    //    BALANCESHEETSTOCKUPDATE("Account", "Balance Sheet Stock Updation", 1.25),
    //    SHOWPARTYDASHBOARDAFTERSELECPARTYINVOUCH("Account", "Show Party Dashboard after selecting Party in Vouchers", 1.26),
    PDC("Account", "Post Dated Cheque", 1.27, "statusDialog", ""),
    //    PACKAGINGUNITOFITEMS("Inventory", "Packaging unit of Items", 2.01),
    //    ALTERNATEUNITOITEMS("Inventory", "Alternate unit of Items", 2.02),
    //    ENABLEMULTIGODOWN("Inventory", "Enable Multi Godown", 2.03),
    //    MANUFACTURINGFEATURES("Inventory", "Enable Manufacturing features", 2.04),
    //    ENABLESALEQUOTATION("Inventory", "Enable Sales Quotation", 2.05),
    //    ENABLEPURCHASEQUOTATION("Inventory", "Enable Purchase Quotation", 2.06),
    //    ENABLEORDERPROCESSING("Inventory", "Enable Order Processing", 2.07),
    //    ENABLESALEPURCHASECHALLAN("Inventory", "Enable Sale/Purchase Challan", 2.08),
    //    CARRYPENDINGMATERIALTONEXTFY("Inventory", "Carry Oending material to next FY", 2.09),
    //    PICKITEMSIZEINFOFROMITMDESCP("Inventory", "Pick Item Size Information from Item Description", 2.11),
    //    SEPERATESTOCKUPDATIONDATEINDUALVOUCH("Inventory", "Seperate Stock Updation Date in Dual Vouchers", 2.12),
    //    SEPERATESTOCKVALUATIONMETHODFORITEMS("Inventory", "Seperate Stock Valuation Method for Items", 2.13),
    //    ACCINPUREINVENTORYVOUCHERS("Inventory", "Accounting In Pure Inventory Vouchers", 2.14),
    //    ENABLEPARTYWISEITEMCODES("Inventory", "Enable party-wise Item Codes", 2.15),
    //    ALLOWSALRETURNINSALESVOUCHER("Inventory", "Allow Sales Return In Sales Voucher", 2.16),
    //    ALLOWPURRETURNINPURVOUCHER("Inventory", "Allow Purchase Return In Purchase Voucher", 2.17),
    //    VALIDATESALRETURNWITHORGVOUCHER("Inventory", "Validate Sales Return with Original Sales", 2.18),
    //    VALIDATEPURRETURNWITHORGVOUCHER("Inventory", "Validate Purchase Return with Original Sales", 2.19),
    //    ENABLEBILLSUNDRYNARRATION("Inventory", "Enable Bill Sundry Narration", 2.21),
    //    INVOICEBARCODEPRINTING("Inventory", "Invoice Bar Code Printing(2D)", 2.22),
    //    ENABLECONSIGNMENTSALES("Inventory", "Enable Consignment Sales", 2.23),
    //    MAINTAINITEMCATEGORY("Inventory", "Maintain Item Category", 2.24),
    //    ENABLESCHEME("Inventory", "Enable Scheme", 2.25),
    //    ENABLEJOBWORK("Inventory", "Enable Job Work", 2.26),
    //    PARAMETERIZEDDETAILS("Inventory", "Parameterized Details", 2.27),
    //    BATCHWISEDETAILS("Inventory", "Batch-Wise Details", 2.28),
    //    SERIALNOWISEDETAILS("Inventory", "Serial No. Wise Details", 2.29),
    //    MRPWISEDETAILS("Inventory", "MRP wise Details", 2.31),
    //    SKIPITEMSDEFAULTPRICEDURVOUCHERMODI("Inventory", "Skip Items Default Price During Voucher Modification", 2.32),
    //    ENABLEFREEQUANTITYINVOUCHERS("Inventory", "Enable Free Quantity In Vouchers", 2.33),
    //    SHOWLASTTRANDURINGSALES("Inventory", "Show Last Transactions during Sales", 2.34),
    //    SHOWLASTTRANDURINGPURCHASE("Inventory", "Show Last Transactions during Purchase", 2.35),
    //    ALLOCATEADDITIONALEXPENSESVOUCHERWISE("Inventory", "Allocate Additional Expenses Voucher-Wise", 2.36),
    //    ALLOCATEEXPENSEPURCHTOITEMS("Inventory", "Allocated Expense/Purch. to Items", 2.37),
    //    MAINTAINIMGNOTESMASTERSVOUCH("Inventory", "Maintain Image/Notes with Masters/Vouchers", 2.38),
    //    SHOWITEMSCURBALDURINGVOUCHENTRY("Inventory", "Show Items Current Balance During Voucher Entry", 2.39),
    //    MAINTAINDRUGLICENSE("Inventory", "Maintain Drug License", 2.41),
    //    ENABLEDATEWISEITEMPRICING("Inventory", "Enable Date Wise Item Pricing", 2.42),
    //    CALCULATEITEMSALEPRICEFORMPURPRICE("Inventory", "Calculate Item Sale Price From Purchase Price", 2.43),
    //    UPDATEITEMPRICESFROMVOUCHER("Inventory", "Update Item Prices From Vouchers", 2.44),
    //    ENABLEPACKINGDETAILSINVOUCHER("Inventory", "Enable Packing Details In Vouchers", 2.45),
    //    QUANTITYDECIMALPLACES("Inventory", "Quantity Decimal Places", 2.46),
    //    ITEMWISEDISCDECIMALPLACES("Inventory", "Item wise Discount Decimal Places", 2.47),
    //    ITEMWISEDISCOUNTTYPE("Inventory", "Item wise Discount Type", 2.48),
    //    STOCKVATMETHOD("Inventory", "Stock VAT Method", 2.49),
    //    TAGSALEPURCACCWITH("Inventory", "Tag Sale/Purchase Acc.with", 2.51),
    //    TAGSTOCKACCWITH("Inventory", "Tag Stock Acc.with", 2.52),
    //    DONOTMAINTAINSTOCKBAL("Inventory", "Donot Maintain Stock Bal", 2.53),
    ITEMWISEMARKUPTYPE("Inventory", "Item Wise Markup Type", 2.54, "statusDialog", ""),
    PARAMETARIZEDSTOCKDETAIL("Inventory", " Parameterized Stock Detail", 2.55, "parameterizedStockDetail", "parameterForm"),
    BARCODECONFIG("Inventory", " Barcode Configuration", 2.56, "barcodeConfig", "barcodeForm"),
    MULTIUNIT("Inventory", "Multi Unit", 2.57, "statusDialog", ""),
    MULTIRATE("Inventory", "Multi Rate", 2.58, "statusDialog", ""),
    MASTERSETUPFORPOS("Inventory", "Master Setup For POS", 2.59, "posModelVar", "posModelForm"),
    ADDITIONALFIELDSETUPFORCRM("CRM", "Additional Field Setup For CRM", 3.01, "crmAdd", "crmMainForm");

    MasterSetupEnum(String pageHead, String pageName, double pageId, String dialog, String formId) {
        this.pageId = pageId;
        this.pageName = pageName;
        this.pageHead = pageHead;
        this.dialog = dialog;
        this.formId = formId;
    }

    private final String pageName;
    private final double pageId;
    private final String pageHead;
    private final String dialog;
    private final String formId;
}
