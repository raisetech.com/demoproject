package com.docGov.utils;


import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Data
public class NepaliDateConverter {

    public Map<Integer, int[]> nepaliMap = new HashMap<>();
    private Map<Integer, String> nepaliMonthMap = new LinkedHashMap<>();
    private Map<String, Integer> nepaliMonthMapreverse = new LinkedHashMap<>();
    private Calendar baseEngDate;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");

    public NepaliDateConverter() {
        nepaliMonthMap.put(1, "Baisakh");
        nepaliMonthMap.put(2, "Jestha");
        nepaliMonthMap.put(3, "Ashadh");
        nepaliMonthMap.put(4, "Shrawan");
        nepaliMonthMap.put(5, "Bhadra");
        nepaliMonthMap.put(6, "Ashwin");
        nepaliMonthMap.put(7, "Kartik");
        nepaliMonthMap.put(8, "Mangsir");
        nepaliMonthMap.put(9, "Poush");
        nepaliMonthMap.put(10, "Magh");
        nepaliMonthMap.put(11, "Falgun");
        nepaliMonthMap.put(12, "Chaitra");

        nepaliMonthMapreverse.put("Baisakh", 1);
        nepaliMonthMapreverse.put("Jestha", 2);
        nepaliMonthMapreverse.put("Ashadh", 3);
        nepaliMonthMapreverse.put("Shrawan", 4);
        nepaliMonthMapreverse.put("Bhadra", 5);
        nepaliMonthMapreverse.put("Ashwin", 6);
        nepaliMonthMapreverse.put("Kartik", 7);
        nepaliMonthMapreverse.put("Mangsir", 8);
        nepaliMonthMapreverse.put("Poush", 9);
        nepaliMonthMapreverse.put("Magh", 10);
        nepaliMonthMapreverse.put("Falgun", 11);
        nepaliMonthMapreverse.put("Chaitra", 12);

        nepaliMap.put(2065, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31});
        nepaliMap.put(2066, new int[]{0, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31});
        nepaliMap.put(2067, new int[]{0, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2068, new int[]{0, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2069, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31});
        nepaliMap.put(2070, new int[]{0, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30});
        nepaliMap.put(2071, new int[]{0, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2072, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2073, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31});
        nepaliMap.put(2074, new int[]{0, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2075, new int[]{0, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2076, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30});
        nepaliMap.put(2077, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31});
        nepaliMap.put(2078, new int[]{0, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2079, new int[]{0, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30});
        nepaliMap.put(2080, new int[]{0, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30});
        nepaliMap.put(2081, new int[]{0, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2082, new int[]{0, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2083, new int[]{0, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2084, new int[]{0, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2085, new int[]{0, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2086, new int[]{0, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2087, new int[]{0, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2088, new int[]{0, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2089, new int[]{0, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30});
        nepaliMap.put(2090, new int[]{0, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30});

        baseEngDate = new GregorianCalendar();
        baseEngDate.set(2013, 9, 18);

    }
    int startingEngYear = 2008;
    int startingEngMonth = 10;
    int startingEngDay = 17;
    int dayOfWeek = Calendar.SUNDAY;
    int startingNepYear = 2065;
    int startingNepMonth = 07;
    int startingNepDay = 1;
    int[] daysInMonth = new int[]{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int[] daysInMonthOfLeapYear = new int[]{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public Date convertToEnglishDateTime(String nepaliDate) {
        Date date = convertToEnglishDate(nepaliDate);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);

        Calendar cal2 = Calendar.getInstance();

        cal1.set(Calendar.HOUR, cal2.get(Calendar.HOUR));
        cal1.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
        cal1.set(Calendar.SECOND, cal2.get(Calendar.SECOND));
        cal1.set(Calendar.MILLISECOND, cal2.get(Calendar.MILLISECOND));

        return cal1.getTime();
    }

    public String currentNepaliMonthInWords(Date date) {
        String s = convertToNepaliDateYearmonth(date);
        String[] str = s.split("-");
        return str[1];
    }

    public Integer nepaliYear(Date date, int year) {
        String s = convertToNepaliDate(date);
        int i = Integer.parseInt(s.substring(0, s.indexOf("-")));
        return i + year;
    }

    public String converToEnglishDateYYYYMMDD(String nepaliDate) {
        Date convertedDate = convertToEnglishDate(nepaliDate);
        return sdf.format(convertedDate);
    }

    public Date convertToEnglishDate(String nepaliDate) {

        startingNepYear = 2065;
        startingNepMonth = 01;
        startingNepDay = 1;

        int engYear = 2008;
        int engMonth = 04;
        int engDay = 13;
//        System.out.println("nepali date kati ho "+nepaliDate);
        String[] array = nepaliDate.split("-", 0);

        int nepYear = Integer.parseInt(array[0]);
        int nepMonth = Integer.parseInt(array[1]);
        int nepDay = Integer.parseInt(array[2]);

        long totalNepDaysCount = 0;

// count total days in-terms of year
        for (int i = startingNepYear; i < nepYear; i++) {
            for (int j = 1; j <= 12; j++) {
                totalNepDaysCount += nepaliMap.get(i)[j];
            }
        }

// count total days in-terms of month
        for (int j = startingNepMonth; j < nepMonth; j++) {
            totalNepDaysCount += nepaliMap.get(nepYear)[j];
        }

// count total days in-terms of date
        totalNepDaysCount += nepDay - startingNepDay;

        int endDayOfMonth = 0;

        while (totalNepDaysCount != 0) {
            if (isLeapYear(engYear)) {
                endDayOfMonth = daysInMonthOfLeapYear[engMonth];
            } else {
                endDayOfMonth = daysInMonth[engMonth];
            }
            engDay++;
            dayOfWeek++;
            if (engDay > endDayOfMonth) {
                engMonth++;
                engDay = 1;
                if (engMonth > 12) {
                    engYear++;
                    engMonth = 1;
                }
            }
            if (dayOfWeek > 7) {
                dayOfWeek = 1;
            }
            totalNepDaysCount--;
        }

        Calendar EngDate = new GregorianCalendar(engYear, engMonth - 1, engDay);

        return EngDate.getTime();

    }

    public static String formatNepaliDate(String date) {
        try {
            if (date != null && !date.isEmpty()) {
                if (date.contains("-")) {
                    String[] str = date.split("-");
                    String finalDate = "";
                    for (String s : str) {
                        if (s.length() == 1) {
                            finalDate += (finalDate.isEmpty() ? "0" + s : "-0" + s);
                        } else {
                            finalDate += (finalDate.isEmpty() ? s : "-" + s);
                        }
                    }
                    return finalDate;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    Boolean dateRepeated;

    public void setDateRepeated(Boolean dateRepeated) {
        this.dateRepeated = dateRepeated;
    }

    public String convertToNepaliDate(Date engDate) {

        baseEngDate.set(2013, 0, 01);

        this.setDateRepeated(false);
        int nepYear = 2069;
        int nepMonth = 9;
        int nepDay = 17;

        Calendar date = new GregorianCalendar();
        date.setTime(engDate);

        int engYear = date.get(Calendar.YEAR);
        int engMonth = date.get(Calendar.MONTH);
        int engDay = date.get(Calendar.DAY_OF_MONTH);

        Calendar currentEngDate = new GregorianCalendar(engYear, engMonth, engDay);

        long totalEngDaysCount = daysBetween(baseEngDate, currentEngDate);

        while (totalEngDaysCount != 0) {
            int daysInIthMonth = nepaliMap.get(nepYear)[nepMonth];

            nepDay++; // incrementing nepali day

            if (nepDay > daysInIthMonth) {
                nepMonth++;
                nepDay = 1;
            }
            if (nepMonth > 12) {
                nepYear++;
                nepMonth = 1;
            }

            dayOfWeek++; // count the days in terms of 7 days
            if (dayOfWeek > 7) {
                dayOfWeek = 1;
            }

            totalEngDaysCount--;
        }

        // String NameOfMonth = nepaliMonthMap.get(nepMonth);
        String NepDate = nepYear + "-" + nepMonth + "-" + nepDay;

        NepDate = nepYear + "-" + nepMonth + "-" + nepDay;

        return formatNepaliDate(NepDate);

    }

    public String convertToNepaliDateYearmonth(Date engDate) {

        baseEngDate.set(2013, 0, 01);

        this.setDateRepeated(false);
        int nepYear = 2069;
        int nepMonth = 9;
        int nepDay = 17;

        Calendar date = new GregorianCalendar();
        date.setTime(engDate);

        int engYear = date.get(Calendar.YEAR);
        int engMonth = date.get(Calendar.MONTH);
        int engDay = date.get(Calendar.DAY_OF_MONTH);

        Calendar currentEngDate = new GregorianCalendar(engYear, engMonth, engDay);

        long totalEngDaysCount = daysBetween(baseEngDate, currentEngDate);

        while (totalEngDaysCount != 0) {
            int daysInIthMonth = nepaliMap.get(nepYear)[nepMonth];

            nepDay++; // incrementing nepali day

            if (nepDay > daysInIthMonth) {
                nepMonth++;
                nepDay = 1;
            }
            if (nepMonth > 12) {
                nepYear++;
                nepMonth = 1;
            }

            dayOfWeek++; // count the days in terms of 7 days
            if (dayOfWeek > 7) {
                dayOfWeek = 1;
            }

            totalEngDaysCount--;
        }

        String NameOfMonth = nepaliMonthMap.get(nepMonth);

        return nepYear + "-" + NameOfMonth + "-" + nepDay;

    }

    public String convertToNepaliDateYYYYmmDD(Date engDate) {

        baseEngDate.set(2013, 0, 01);

        this.setDateRepeated(false);
        int nepYear = 2069;
        int nepMonth = 9;
        int nepDay = 17;

        Calendar date = new GregorianCalendar();
        date.setTime(engDate);

        int engYear = date.get(Calendar.YEAR);
        int engMonth = date.get(Calendar.MONTH);
        int engDay = date.get(Calendar.DAY_OF_MONTH);

        Calendar currentEngDate = new GregorianCalendar(engYear, engMonth, engDay);

        long totalEngDaysCount = daysBetween(baseEngDate, currentEngDate);

        while (totalEngDaysCount != 0) {
            int daysInIthMonth = nepaliMap.get(nepYear)[nepMonth];

            nepDay++; // incrementing nepali day

            if (nepDay > daysInIthMonth) {
                nepMonth++;
                nepDay = 1;
            }
            if (nepMonth > 12) {
                nepYear++;
                nepMonth = 1;
            }

            dayOfWeek++; // count the days in terms of 7 days
            if (dayOfWeek > 7) {
                dayOfWeek = 1;
            }

            totalEngDaysCount--;
        }

        String NameOfMonth = nepaliMonthMap.get(nepMonth);
        String NepDate = nepYear + "-" + nepMonth + "-" + nepDay;

        NepDate = nepYear + "-" + nepMonth + "-" + nepDay;

        return NepDate;

    }

    public static int getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        int diffInMillies = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
        return diffInMillies;
        // return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static int getDateDiff(Date date1, Date date2, boolean countToday) {
        int diffInMillies = (int) ((date2.getTime() - date1.getTime()) / (24 * 60 * 60 * 1000));
        return (diffInMillies+1);
        // return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }


    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public long daysBetween(Calendar startDate, Calendar endDate) {

        Calendar date = (Calendar) startDate.clone();
        long daysBetween = 0;
        while (date.before(endDate)) {
            date.add(Calendar.DAY_OF_MONTH, 1);

            daysBetween++;
        }
        return daysBetween;
    }

    public static boolean isLeapYear(int year) {
        if (year % 100 == 0) {
            return year % 400 == 0;
        } else {
            return year % 4 == 0;
        }
    }

    public int getNepaliYear(Date date) {
        return nepaliDatePartial(0, date);
    }

    public int getNepaliMonth(Date date) {
        return nepaliDatePartial(1, date);
    }

    public int getNepaliDay(Date date) {
        return nepaliDatePartial(2, date);
    }

    public String currentNepaliDate() {
        return convertToNepaliDateYYYYmmDD(new Date());
    }

    public String covertNepaliDate(Date date) {
        return convertToNepaliDateYYYYmmDD(date);
    }

    public Integer lastDayOfMonth(Date date) {
        try {
            int[] j = nepaliMap.get(getNepaliYear(date));
            return j[getNepaliMonth(date)];
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Date dateFromNepaliMonth(String month, boolean startDate) {
        try {
            int monthInNumber = nepaliMonthMapreverse.get(month);
            int year = getNepaliYear(new Date());
            if (startDate) {
                return convertToEnglishDate(year + "-" + monthInNumber + "-" + 1);
            } else {
                int[] j = nepaliMap.get(year);
                int lastDayOfMonth = j[monthInNumber];
                return convertToEnglishDate(year + "-" + monthInNumber + "-" + lastDayOfMonth);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer lastDayofMonth(String nepaliDate) {
        try {
            String[] str = nepaliDate.split("-");
            int[] j = nepaliMap.get(Integer.parseInt(str[0]));
            return j[Integer.parseInt(str[1])];
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Integer lastDayofMonth(Date date) {
        try {
            String nepaliDate = convertToNepaliDateYYYYmmDD(date);
            String[] str = nepaliDate.split("-");
            int[] j = nepaliMap.get(Integer.parseInt(str[0]));
            return j[Integer.parseInt(str[1])];
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Integer nepaliDatePartial(int i, Date date) {
        String s = convertToNepaliDate(date);
        String[] str = s.split("-");

        if (i == 0) {
            return Integer.parseInt(str[0]);

        } else if (i == 1) {
            return Integer.parseInt(str[1]);

        } else if (i == 2) {
            return Integer.parseInt(str[2]);
        }

        return null;
    }

    public double calculateDurationBetweenTwoTimes(Date fromDate, Date toDate){
        if (fromDate != null){
            if(toDate != null){
                String fromTime = localDateFormat.format(fromDate);
                String toTime = localDateFormat.format(toDate);
                String[] time1 = fromTime.split(":");
                String[] time2 = toTime.split(":");
                int hours1 = Integer.parseInt(time1[0], 10);
                int hours2 = Integer.parseInt(time2[0], 10);
                int mins1 = Integer.parseInt(time1[1], 10);
                int mins2 = Integer.parseInt(time2[1], 10);
                double hours = hours2 - hours1;
                double mins = 0;
                if (hours < 0)
                    hours += 24;
                if(mins2 >= mins1)
                    mins = mins2 - mins1;
                else {
                    mins = (mins2 + 60) - mins1;
                    hours--;
                }
                mins = mins / 60; // take percentage in 60
                hours += mins;
                hours = Math.round(hours * 100.0) / 100.0;
                return hours;
            }
        }
        return 0.0;
    }

    public List<Date> getDatesBetweenTwoDates(Date startDate, Date endDate){
        List<Date> dates = new ArrayList<>();
        try {
            long interval = 24*1000 * 60 * 60; // 1 hour in millis
            long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
            long curTime = startDate.getTime();
            while (curTime < endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
        }catch (Exception e) {
            Logger.getLogger(NepaliDateConverter.class.getName()).log(Level.SEVERE, null, e);
        }
        return dates;
    }

    public String nepaliMonthInWords(Date date) {
        String s = convertToNepaliDateYearmonth(date);
        String[] str = s.split("-");
        return str[1];
    }

    public Map<Integer, String> getNepaliMonthMap() {
        return nepaliMonthMap;
    }

    public void setNepaliMonthMap(Map<Integer, String> nepaliMonthMap) {
        this.nepaliMonthMap = nepaliMonthMap;
    }

    public Map<String, Integer> getNepaliMonthMapreverse() {
        // System.out.println("nepalimonthreverse " + nepaliMonthMapreverse);
        return nepaliMonthMapreverse;
    }

    public void setNepaliMonthMapreverse(Map<String, Integer> nepaliMonthMapreverse) {
        this.nepaliMonthMapreverse = nepaliMonthMapreverse;
    }
}
