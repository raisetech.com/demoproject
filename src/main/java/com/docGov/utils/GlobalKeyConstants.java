package com.docGov.utils;

import lombok.Getter;

@Getter
public class GlobalKeyConstants {

    public static final long defaultOrganisationId = 1;
    public static final String invalidUser = "Invalid User credentials";
    public static final String createdMessage = " Created successfully!";
    public static final String InquiryMessage = " Your request have been received successfully!";
    public static final String EventMessage = " Your registration have been received successfully!";
    public static final String updatedMessage = " Updated successfully!";
    public static final String deletedMessage = " Deleted successfully!";
    public static final String alreadyExistMessage = " Already exist!";
    public static final String reportGenerated = " Report generated successfully";

    public static final String listNotFound = "Sorry! list not found!";

    public static final String notFoundMessage = "Data not exist!";
    public static final String notValidationMessage = " Data must not be empty!";
    public static final String listFound = "List found!";

    public static final String TransactionSerialNo = "SerialNo";
    //For Live
    public static final String imageViewLocation = "http://nepaldigitalprofile.com/images/";
    //For Local
//    public static final String imageViewLocation = "/home/zordan/images/images/";


    public static final String cannotBeEmpty = "Cannot be Empty!";
    //For Live
    public static final String imageLocation = "/var/rtech_images/";
    //Fot local
//    public static final String imageLocation = "/home/zordan/images/images/";

//    public static final String imageLocation_locale = "F:\\imageecalculo";
    public static final String DefaultJournalTransactionModuleType = "JV";
    public static final String DefaultReceiptTransactionModuleType = "RV";
    public static final String DefaultSalaryPostingTransactionModuleType = "SP";
    public static final String DefaultPaymentTransactionModuleType = "PV";
    public static final String DefaultReversalTransactionModuleType = "REV";
    public static final String DefaultCreditNoteTransactionModuleType = "CNV";
    public static final String DefaultDebitNoteTransactionModuleType = "DNV";
    public static final String DefaultCashDepositTransactionModuleType = "CHD";
    public static final String DefaultBankTransferTransactionModuleType = "BHW";
    public static final String DefaultCashWithdrawalTransactionModuleType = "CWD";
    public static final String DefaultPurchaseOrderTransactionModuleType = "POV";
    public static final String DefaultPurchaseReturnTransactionModuleType = "PRV";
    public static final String DefaultPurchaseChallanTransactionModuleType = "PCV";
    public static final String DefaultPurchaseInvoiceTransactionModuleType = "PIV";
    public static final String DefaultSalesOrderTransactionModuleType = "SOV";
    public static final String DefaultSalesChallanTransactionModuleType = "SCV";
    public static final String DefaultSalesInvoiceTransactionModuleType = "SIV";
    public static final String DefaultSalesReturnTransactionModuleType = "SRV";
    public static final String DefaultConsumptionTransactionModuleType = "CONV";
    public static final String DefaultDamageTransactionModuleType = "DMGV";
    public static final String DefaultFixedAssetsSalesInvoiceTransactionModuleType = "FSIV";
    public static final String DefaultFixedAssetsPurchaseInvoiceTransactionModuleType = "FPIV";


    public static final String DefaultJournalModuleType = "jou";
    public static final String DefaultReceiptModuleType = "rec";
    public static final String DefaultPaymentModuleType = "pay";
    public static final String DefaultReversalModuleType = "reversal";
    public static final String DefaultDebitNoteModuleType = "debNote";
    public static final String DefaultCreditNoteModuleType = "crdNote";
    public static final String DefaultCashDepositModuleType = "cashDeposit";
    public static final String DefaultBankTransferModuleType = "bankTransfer";
    public static final String DefaultCashWithdrawalModuleType = "cashWithdraw";
    public static final String DefaultPurchaseChallanModuleType = "purChallan";
    public static final String DefaultPurchaseOrderModuleType = "purOrder";
    public static final String DefaultPurchaseFixedAssetsInvoiceModuleType = "faPurInvoice";
    public static final String DefaultFixedAssetsPurchaseChallanModuleType = "faPurChallan";
    public static final String DefaultFixedAssetsPurchaseOrderModuleType = "faPurOrder";
    public static final String DefaultFixedAssetsSalesInvoiceModuleType = "faSalesInvoice";
    public static final String DefaultFixedAssetsSalesChallanModuleType = "faSalesChallan";
    public static final String DefaultFixedAssetsSalesOrderModuleType = "faSalesOrder";
    public static final String DefaultPurchaseInvoiceModuleType = "purInvoice";
    public static final String DefaultPurchaseReturnModuleType = "purReturn";
    public static final String DefaultSalesChallanModuleType = "salesChallan";
    public static final String DefaultSalesOrderModuleType = "salesOrder";
    public static final String DefaultSalesInvoiceModuleType = "salesInvoice";
    public static final String DefaultConsumptionModuleType = "consumption";
    public static final String DefaultDamageModuleType = "damage";
    public static final String DefaultSalesReturnModuleType = "salesReturn";
    public static final String DefaultSalaryPostingModuleType = "salaryPosting";




    public static final String DefaultPurchaseInvoiceRemarks = "Purchase bill no ";
    public static final String DefaultPurchaseOrderRemarks = "Purchase Order no ";
    public static final String DefaultPurchaseReturnRemarks = "Purchase Return no ";
    public static final String DefaultPurchaseChallanRemarks = "Purchase Challan no ";
    public static final String DefaultSalesOrderRemarks = "Sales Order no ";
    public static final String DefaultSalesReturnRemarks = "Sales Return no ";
    public static final String DefaultSalesChallanRemarks = "Sales Challan no ";
    public static final String DefaultSalesInvoiceRemarks = "Sales Invoice no ";
    public static final String DefaultStockTransforRemarks = "Stock Transfor no ";
    public static final String DefaultConsumptionRemarks = "Consumption Order no ";
    public static final String DefaultDamageRemarks = "Damage Order no ";
    public static final String DefaultEmployeeInventoryRemarks = "Inventory Assign no ";
    public static final String DefaultFinishedGoodsRemarks = "Finished Goods no ";


    //Accounting Default remarks
    public static final String DefaultJournalRemarks = "Journal voucher no ";
    public static final String DefaultPaymentRemarks = "Payment voucher no ";
    public static final String DefaultReceiptRemarks = "Receipt voucher no ";
    public static final String DefaultDebitNoteRemarks = "Debit Note voucher no ";
    public static final String DefaultCreditNoteRemarks = "Credit Note voucher no ";
    public static final String DefaultCashDepositRemarks = "Cash Deposit voucher no ";
    public static final String DefaultCashWithdrawalRemarks = "Cash Withdrawal voucher no ";
    public static final String DefaultBankTransferRemarks = "Bank Transfer voucher no ";



    public static final String PaymentMethodCash = "CASH";
    public static final String PaymentMethodCredit = "CREDIT";

    public static final String BasicAccountSetupForPurchaseInCode = "Purchase Invoice";
    public static final String BasicAccountSetupForPurchaseInDesc = "Purchase Account";

    public static final String BasicAccountSetupForSalesInCode = "Sales Invoice";
    public static final String BasicAccountSetupForSalesInDesc = "Sales Account";

    public static final String BasicAccountSetupForVatPayableInCode = "Vat Payable";
    public static final String BasicAccountSetupForVatPayableInDesc = "Vat Payable";

    public static final String BasicAccountSetupForVatReceivableInCode = "Vat Receivable";
    public static final String BasicAccountSetupForVatReceivableInDesc = "Vat Receivable";

    public static final String BasicAccountSetupForDiscountPayableInCode = "Discount Payable";
    public static final String BasicAccountSetupForDiscountPayableInDesc = "Discount Payable";

    public static final String BasicAccountSetupForDiscountReceivableInCode = "Discount Receivable";
    public static final String BasicAccountSetupForReceivableInDesc = "Discount Receivable";

    public static final int AccountSubHeadCodeCash = 1101;
    public static final int AccountSubHeadCodeBank = 1102;
    public static final int AccountSubHeadCodeSupplier = 2103;
    public static final int AccountSubHeadCodeCustomer = 1107;
    public static final int  AccountSubHeadCodePurchase = 3101;
    public static final int  AccountSubHeadCodeSales = 4101;
    public static final int  AccountSubHeadCodeBankODAccounts = 2105;



    public static final String SUPPLIERACCOUNT = "supplierAcc";
    public static final String CUSTOMERACCOUNT = "customerAcc";





    public static final Long OfficeAccount = 1l;
    public static final Long BankAccount = 2l;
    public static final Long SupplierAccount = 3l;
    public static final Long CustomerAccount = 4l;


    public static final long AssetsNature=1;
    public static final long LiabilitiesNature=2;
//    public static final long EquityNature=3;
    public static final long IncomeNature=4;
    public static final long ExpensesNature=3;



    public static final String DefaultPurchaseAccountName = "Purchase";
    public static final int DefaultPurchaseAccountNumber = 31010001;

    public static final String DefaultProfitAndLossAccountAccountName = "Profit & Loss Account";
    public static final int DefaultProfitAndLossAccountNumber = 32010001;

    public static final String DefaultSalesAccountName = "Sales";
    public static final int DefaultSalesAccountNumber = 41010001;

    public static final String DefaultEsewaAccountName = "ESEWA";
    public static final String DefaultFonepayAccountName = "FONEPAY";
    public static final String DefaultBankAccountName = "BANK";
    public static final String DefaultKhaltiAccountName = "KHALTI";

    public static final String DefaultCashAccountName = "Cash";
    public static final int DefaultCashAccountNumber = 11010001;

    public static final String DefaultVatAccountName = "VAT";
    public static final int DefaultVatAccountNumber = 21010001;

    public static final String ALIGNMENT_LEFT = "Left";
    public static final String ALIGNMENT_RIGHT = "Right";
    public static final String ALIGNMENT_CENTER = "Center";
    public static final String ALIGNMENT_TOP = "Top";
    public static final String ALIGNMENT_DOWN = "Down";


    public static final double fatPercentageRate =5.80;

    public static final double snfPercentageRate =2.95;

    public static final String password="$2a$10$j5.4svTCGL94edXsb9ciCevCc.8pd3C.uTF1GLcCqzVkTemVdsl/6";

    public static final String EARNING = "Earning";
    public static final String DEDUCTION = "Deduction";
}
