package com.docGov.utils;


import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XSSFHelperUtil {

    public static XSSFCellStyle getBorderStyle(XSSFWorkbook wb, short fontStyle, String alignment, short borderCellStyle) {
        XSSFCellStyle borderStyle = getBorderStyle(wb, alignment, borderCellStyle);
        Font boldFont = wb.createFont();
        boldFont.setBoldweight(fontStyle);
        borderStyle.setFont(boldFont);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyle(XSSFWorkbook wb, short fontStyle, String alignment) {
        XSSFCellStyle borderStyle = getBorderStyle(wb, alignment, XSSFCellStyle.BORDER_THIN);
        Font boldFont = wb.createFont();
        boldFont.setBoldweight(fontStyle);
        borderStyle.setFont(boldFont);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyle(XSSFWorkbook wb, String alignment) {
        XSSFCellStyle borderStyle = getBorderStyle(wb, alignment, XSSFCellStyle.BORDER_THIN);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStylethick(XSSFWorkbook wb, String alignment) {
        XSSFCellStyle borderStyle = getBorderStyle(wb, alignment, XSSFCellStyle.BORDER_THICK);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyle(XSSFWorkbook wb, String alignment, short borderCellStyle) {

        XSSFCellStyle borderStyle = wb.createCellStyle();

        textAligner(alignment, borderStyle);
        borderStyle(borderStyle, borderCellStyle);

        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyleThickParticular(XSSFWorkbook wb, String thickAlign) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAlign);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyleThickParticular(XSSFWorkbook wb, List<String> thickAligns) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        return borderStyle;
    }

    public static XSSFCellStyle getBorderStyleThickParticular(XSSFWorkbook wb, List<String> thickAligns, String alignment) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        textAligner(alignment, borderStyle);
        return borderStyle;
    }

    public static void borderStyleThickParticular(XSSFCellStyle borderStyle, String thickAlign) {

        switch (thickAlign) {
            case GlobalKeyConstants.ALIGNMENT_LEFT:
                borderStyle.setBorderLeft(XSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_RIGHT:
                borderStyle.setBorderRight(XSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_TOP:
                borderStyle.setBorderTop(XSSFCellStyle.BORDER_THICK);
                break;

            case GlobalKeyConstants.ALIGNMENT_DOWN:
                borderStyle.setBorderBottom(XSSFCellStyle.BORDER_THICK);
                break;
        }

        List<String> alignPost = new ArrayList<>();
        alignPost.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_RIGHT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_TOP);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_DOWN);

        for (String str : alignPost) {
            if (!str.equals(thickAlign)) {
                switch (str) {
                    case GlobalKeyConstants.ALIGNMENT_LEFT:
                        borderStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_RIGHT:
                        borderStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_TOP:
                        borderStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_DOWN:
                        borderStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
                        break;
                }
            }
        }
    }

    public static void borderStyleThickParticular(XSSFCellStyle borderStyle, List<String> thickAligns) {

        List<String> alignPost = new ArrayList<>();
        alignPost.add(GlobalKeyConstants.ALIGNMENT_LEFT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_RIGHT);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_TOP);
        alignPost.add(GlobalKeyConstants.ALIGNMENT_DOWN);

        for (String str : thickAligns) {

            switch (str) {
                case GlobalKeyConstants.ALIGNMENT_LEFT:
                    borderStyle.setBorderLeft(XSSFCellStyle.BORDER_THICK);
                    break;

                case GlobalKeyConstants.ALIGNMENT_RIGHT:
                    borderStyle.setBorderRight(XSSFCellStyle.BORDER_THICK);
                    break;

                case GlobalKeyConstants.ALIGNMENT_TOP:
                    borderStyle.setBorderTop(XSSFCellStyle.BORDER_THICK);
                    break;

                case GlobalKeyConstants.ALIGNMENT_DOWN:
                    borderStyle.setBorderBottom(XSSFCellStyle.BORDER_THICK);
                    break;
            }

        }

        for (String str : alignPost) {
            if (!thickAligns.contains(str)) {
                switch (str) {
                    case GlobalKeyConstants.ALIGNMENT_LEFT:
                        borderStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_RIGHT:
                        borderStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_TOP:
                        borderStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
                        break;

                    case GlobalKeyConstants.ALIGNMENT_DOWN:
                        borderStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
                        break;
                }
            }
        }
    }

    public static void borderStyle(XSSFCellStyle borderStyle, short borderCellStyle) {
        borderStyle.setBorderBottom(borderCellStyle);
        borderStyle.setBorderTop(borderCellStyle);
        borderStyle.setBorderRight(borderCellStyle);
        borderStyle.setBorderLeft(borderCellStyle);
    }

    public static void textAligner(String alignment, XSSFCellStyle borderStyle) {
        switch (alignment) {
            case GlobalKeyConstants.ALIGNMENT_CENTER:
                borderStyle.setAlignment(CellStyle.ALIGN_CENTER);
                break;
            case GlobalKeyConstants.ALIGNMENT_LEFT:
                borderStyle.setAlignment(CellStyle.ALIGN_LEFT);
                break;
            case GlobalKeyConstants.ALIGNMENT_RIGHT:
                borderStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                break;
        }
    }

    public static CellStyle getBorderStyle1(XSSFWorkbook wb, short borderCellStyle, String alignment, List<String> thickAligns) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        borderStyle.setBorderTop(borderCellStyle);
        textAligner(alignment, borderStyle);
        return borderStyle;
    }

    public static CellStyle getBorderStyleThickParticular1(XSSFWorkbook wb, short BOLDWEIGHT_BOLD, String alignment, List<String> thickAligns) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
        textAligner(alignment, borderStyle);
        return borderStyle;
    }

    public static CellStyle getBorderStyleClosing(XSSFWorkbook wb, short borderCellStyle, List<String> thickAligns, short BORDER_THICK) {
        XSSFCellStyle borderStyle = wb.createCellStyle();
        borderStyleThickParticular(borderStyle, thickAligns);
//        textAligner(alignment, borderStyle);
        borderStyle(borderStyle, borderCellStyle);
        borderStyle.setBorderTop(BORDER_THICK);
        return borderStyle;
    }
}

