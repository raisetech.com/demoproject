package com.docGov.mapper;

import com.docGov.dto.ModulePackageDTO;
import com.docGov.models.ModulePackage;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ModulePackageMapper extends CommonMapper<ModulePackage, ModulePackageDTO> {



    ModulePackageDTO toDTO(ModulePackage modulePackage);

    ModulePackage fromDTO(ModulePackageDTO modulePackageDTO);

    @Override
    List<ModulePackageDTO> toDTOList(List<ModulePackage> modulePackages);

    @Override
    List<ModulePackage> fromDTOList(List<ModulePackageDTO> modulePackageDTOS);
}
