package com.docGov.mapper;

import com.docGov.dto.RoleDTO;
import com.docGov.models.Role;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper extends CommonMapper<Role, RoleDTO>{

    @Named("min")
    @Mappings({
            @Mapping(source = "modules", target = "modules")
    })
    RoleDTO toDTO(Role role);


    @Mappings({
    })
    Role fromDTO(RoleDTO roleDTO);


    @IterableMapping(qualifiedByName = "toDTOList")
    List<RoleDTO> toDTOList(List<Role> role);


    List<Role> fromDTOList(List<RoleDTO> roleDTOS);
}
