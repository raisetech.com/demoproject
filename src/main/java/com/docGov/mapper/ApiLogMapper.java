package com.docGov.mapper;

import com.docGov.dto.ApiLogDTO;
import com.docGov.models.ApiLogs;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;

import java.text.ParseException;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ApiLogMapper extends CommonMapper<ApiLogs, ApiLogDTO>{

    @Mappings({
    })

    ApiLogs fromDTO(ApiLogDTO ApiLogsDTO);

    ApiLogDTO toDTO(ApiLogs ApiLogs);

    @Override
    List<ApiLogDTO> toDTOList(List<ApiLogs> ApiLogs);

    @Override
    List<ApiLogs> fromDTOList(List<ApiLogDTO> ApiLogsDTO) throws ParseException;
}
