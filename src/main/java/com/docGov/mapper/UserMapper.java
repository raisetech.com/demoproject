package com.docGov.mapper;

import com.docGov.dto.UserDTO;
import com.docGov.models.User;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring",uses = RoleMapper.class)
public interface UserMapper extends CommonMapper<User, UserDTO> {


    @Named("min")
    @Mappings({
            //Array of Singular Mapping annotation
            @Mapping(source = "user.roleId.id", target = "roleId"),
            @Mapping(source = "roleId", target = "role"),
//            @Mapping(source = "user.fyId.id", target = "fyId"),
//            @Mapping(source = "createdDate", target = "createdDate", ignore = true),
            @Mapping(source = "contactNumber",target = "contactNumber"),
            @Mapping(source = "user.password", target = "password", ignore = true)
//            @Mapping(source = "user.warehouseId", target = "warehouseId",ignore = true)

    })
    UserDTO toDTO(User user);


    @Mappings({
//            @Mapping(source = "fyId", target = "fyId.id"),
            @Mapping(source = "roleId", target = "roleId.id")
    })
    User fromDTO(UserDTO userDTO);


    @IterableMapping(qualifiedByName = "toDTOList")
    List<UserDTO> toDTOList(List<User> users);


    List<User> fromDTOList(List<UserDTO> userDTOS);
}
