package com.docGov.exceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalException extends Exception {
    private HttpStatus status;
    private String message;


    public GlobalException(String s) {
    }
}
