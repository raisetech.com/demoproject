/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.19-MariaDB : Database - docGov
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`docGov` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `docGov`;

/*Table structure for table `api_logs` */

DROP TABLE IF EXISTS `api_logs`;

CREATE TABLE `api_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `delete_by` bigint(20) DEFAULT NULL,
  `delete_reason` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `entered_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `api_path` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `response_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `api_logs` */

/*Table structure for table `module_package` */

DROP TABLE IF EXISTS `module_package`;

CREATE TABLE `module_package` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `delete_by` bigint(20) DEFAULT NULL,
  `delete_reason` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `entered_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `module_contains` varchar(255) DEFAULT NULL,
  `modules_enums` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `module_package` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `delete_by` bigint(20) DEFAULT NULL,
  `delete_reason` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `entered_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `modules` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK97mxvrajhkq19dmvboprimeg1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`created_date`,`delete_by`,`delete_reason`,`deleted`,`deleted_date`,`entered_by`,`updated_date`,`modules`,`name`,`user_id`) values (1,NULL,NULL,NULL,0,NULL,NULL,NULL,'5.01,5.02,','Admin',NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `delete_by` bigint(20) DEFAULT NULL,
  `delete_reason` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `entered_by` bigint(20) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `current_connections` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `del_date` datetime DEFAULT NULL,
  `del_reason` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `is_admin` bit(1) DEFAULT NULL,
  `is_pos_user` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `ref_org_id` bigint(20) DEFAULT NULL,
  `ref_user_id` bigint(20) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `riders_admin` bit(1) DEFAULT NULL,
  `total_connections` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `ward_ids` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp56c1712k691lhsyewcssf40f` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`created_date`,`delete_by`,`delete_reason`,`deleted`,`deleted_date`,`entered_by`,`updated_date`,`confirmation_token`,`contact_number`,`created_by`,`current_connections`,`customer_id`,`del_date`,`del_reason`,`email`,`enabled`,`is_admin`,`is_pos_user`,`name`,`password`,`ref_org_id`,`ref_user_id`,`reset_token`,`riders_admin`,`total_connections`,`updated_by`,`username`,`ward_ids`,`role_id`) values (1,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Demo','$2a$10$j5.4svTCGL94edXsb9ciCevCc.8pd3C.uTF1GLcCqzVkTemVdsl/6',NULL,NULL,NULL,NULL,NULL,NULL,'demo',NULL,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
